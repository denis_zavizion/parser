﻿function addCheckboxDatepicker() {
  setTimeout(function() {
    jQuery(".ui-datepicker-calendar td").filter(function() {
      	var date = $(this).text();
      	return /\d/.test(date);
    }).each(function(){
		if (!jQuery(this).find('.datepicker-checkbox').length) {
			jQuery(this).find("a, span").after("<span class='datepicker-checkbox'>");
		}
	});
  }, 0)
}

jQuery(document).ready(function($) {
	
	$('input[type=checkbox], input[type=radio]').ezMark();
	
	addCheckboxDatepicker();
	
	$('#navmobile-btn').click(function(){
		if ($('body').hasClass('menu-opened')) {
			$('.main-menu').slideUp('fast');
			$('body').removeClass('menu-opened');
		} else {
			$('.main-menu').slideDown('fast');	
			$('body').addClass('menu-opened');
		}
	})
	
	$('.percent-updater').click(function(){
		if ($(this).hasClass('active')) {
			$($(this).data('modal')).modal('show');
		} else {
			$(this).closest('.percent-item').addClass('loading');
			if ($('.services-list').length){
				$('.services-list').find('.percent-service').addClass('loading');
			}
		}
	})
	
	$('.update-check .ez-checkbox input').click(function(){
		if ($(this).is(':checked')) {
			$(this).closest('.update-item').addClass('safe').removeClass('unsafe');
		} else {
			$(this).closest('.update-item').removeClass('safe').addClass('unsafe');
		}
	})
	
	$('.results-table .result-check .ez-checkbox input').click(function(){
		if ($(this).is(':checked')) {
			$(this).closest('tr').find('.result-item').addClass('safe').removeClass('unsafe');
		} else {
			$(this).closest('tr').find('.result-item').removeClass('safe').addClass('unsafe');
		}
	})
	
	var clipboard = new Clipboard('.btn-clipboard');

	
	$('.change-circle').click(function(){
		var text = $(this).text().trim();
		var vals = $(this).data('values').split(",");
		var ind;
		for (var i = 0; i < vals.length; i++) {
			if (vals[i]==text) ind = i;
		}
		if (ind==vals.length-1) {
			$(this).text(vals[0]);
		} else if (typeof ind !== "undefined") {
			$(this).text(vals[ind+1]);
		}
		return false;
	})
	
	$('.timepicker').timepicker({
		timeOnlyTitle: 'Выберите время',
		currentText: 'Сейчас',
		closeText: 'Закрыть'
	});
	
});