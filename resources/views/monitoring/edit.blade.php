@extends('app')

@section('content')
    @include('menu', ['page' => 'monitoring.new'])
    <div id="container">
        <div class="form-block">
            {!! Form::model($monitoring, ['method' => 'POST', 'route' => ['monitoring.edit.post', $monitoring->id]]) !!}
                @include('monitoring/partials/_form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection