@extends('app')

@section('content')
    @include('menu', ['page' => 'user.index'])
    <div id="container">
        <div class="top-actions">
            <a href="{{ route('user.new') }}" class="btn btn-second"><i class="fa fa-user-plus"></i> Добавить пользователя</a>
        </div>
        <table class="data-table settings-table">
            <thead>
                <tr>
                    <th class="a-left">Имя</th>
                    <th class="a-left nowrap">О себе</th>
                    <th class="a-left">E-mail</th>
                    <th class="a-left">Токен</th>
                    <th class="a-left">Роль</th>
                    <th class="a-left">Статус</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                    <tr>
                        <td data-th="Имя">{{ $user->login }}</td>
                        <td data-th="О себе">{{ $user->cv }}</td>
                        <td data-th="E-mail">{{ $user->email }}</td>
                        <td data-th="Токен">{{ $user->telegram->token }}</td>
                        <td data-th="Роль">

                            @if($user->isAdmin())
                                <a href="#" class="change-circle ajax-role"
                                   data-values="admin,user"
                                   data-user_id="{{ $user->id }}"
                                        >admin</a>
                            @else
                                <a href="#" class="change-circle ajax-role"
                                   data-values="admin,user"
                                   data-user_id="{{ $user->id }}"
                                        >user</a>
                            @endif

                        </td>
                        <td data-th="Статус">

                            @if($user->status)
                                <a href="#" class="change-circle ajax-status"
                                   data-values="active,inactive"
                                   data-user_id="{{ $user->id }}"
                                        >active</a>
                            @else
                                <a href="#" class="change-circle ajax-status"
                                   data-values="active,inactive"
                                   data-user_id="{{ $user->id }}"
                                        >inactive</a>
                            @endif

                        </td>
                        <td class="table-actions">
                            @if(!$user->isAdmin())
                            <a onclick="return confirm('Вы уверены?')" href="{{ route('user.delete', $user->id) }}" class="remove-link"></a>
                            @endif
                            <a href="{{ route('user.edit', $user->id) }}" class="add-link"><i class="fa fa-gears"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $users->render() !!}
    </div>
@endsection

@push('scripts')

    <script type="text/javascript">
        $(".ajax-status").click(function () {
            var user_id = $(this).data('user_id');
            var val =  $(this).html();
            $.ajax({url: '{{route('user.status.reset')}}' + '?user_id=' + user_id + '&val='+ val, success: function(result){

            }});
        });
        $(".ajax-role").click(function () {
            var user_id = $(this).data('user_id');
            var val =  $(this).html();
            $.ajax({url: '{{route('user.role.ajax')}}' + '?user_id=' + user_id + '&val='+ val, success: function(result){

            }});
        });

    </script>

@endpush