<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{

    public function index()
    {
        $countries =  Country::orderBy('country', 'ASC')
            ->paginate(10)
        ;

        return view('country.index', compact('countries'));

    }

    public function ajaxPresetReset(Request $request)
    {
        $country_id = $request->country_id;
        $val = $request->val;

        try{

            $country = Country::where(['id' => $country_id])->first();
            $country->preset = $val;
            $country->save();

        }
        catch(\Exception $e){
            $message = $e->getMessage();
            \Log::error('CountryController ajaxPresetReset'.$message);
        }

        return \Response::json('Ok');

    }
}
