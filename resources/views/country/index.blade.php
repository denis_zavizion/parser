@extends('app')

@section('content')
    @include('menu', ['page' => 'country.index'])
    <div id="container">
        <div class="top-actions">
        </div>
        <table class="data-table settings-table">
            <thead>
                <tr>
                    <th class="a-left">Имя</th>
                    <th class="a-left">Прокси чекер </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($countries as $key => $country)
                    <tr>
                        <td data-th="Имя">{{ $country->country }}</td>

                        <td data-th="Пресет">
                            <input type="text"
                                   id="input-preset-{{ $country->id }}"
                                   class = "input-preset"
                                   data-country_id="{{ $country->id }}"
                                   name="input-preset-{{ $country->id }}"
                                   value="{{ $country->proxy_preset }}">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $countries->render() !!}
    </div>
@endsection

@push('scripts')

    <script type="text/javascript">
        $(".input-preset").change(function() {
            var country_id = $(this).data('country_id');
            var val = $(this).val();
            $.ajax({url: '{{route('country.preset.reset')}}' + '?country_id=' + country_id + '&val='+ val, success: function(result){
                    location.reload();
            }});
        });


    </script>

@endpush