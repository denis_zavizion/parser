<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StringGo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('string_go', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('string_id')->unsigned();
            $table->json('json_string')->nullable();
            $table->timestamp('last_success_request_date')->nullable();
            $table->timestamp('next_request_date')->nullable();

            $table->index('user_id');
            $table->index('string_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('string_id')->references('id')->on('table_all_strings');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
     {
         Schema::table('string_go', function (Blueprint $table) {
            $table->dropForeign('string_go_user_id_foreign');
            $table->dropForeign('string_go_string_id_foreign');
        });
        Schema::dropIfExists('string_go');
    }
}
