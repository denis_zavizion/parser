<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title>Мониторинг</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic"
          rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui-timepicker-addon.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui.multidatespicker.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/js/jquery-ui-timepicker-addon.js') }}"></script>
    <script src="{{ asset('/js/jquery-ui.multidatespicker.js') }}"></script>
    <script src="{{ asset('/js/highcharts.js') }}"></script>
    <script src="{{ asset('/js/clipboard.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.ezmark.js') }}"></script>
    <script src="{{ asset('/js/modal.js') }}"></script>
    <script src="{{ asset('/js/theme-script.js') }}"></script>
</head>
<body>
<div id="wrapper">
    <div class="center">
        @yield('content')
    </div>
</div>
<!-- end wrapper -->
@yield('modal')

<script type="text/javascript">
    var link_id;
    var min_date;
    var monitoring_id;
    var status;

    var chart ;

    function check() {
        setTimeout(function() {
            jQuery(".ui-datepicker-calendar td").filter(function() {
                var date = $(this).text();
                return /\d/.test(date);
            }).each(function(){
                if (!jQuery(this).find('.datepicker-checkbox').length) {
                    jQuery(this).find("a, span").after("<span class='datepicker-checkbox'>");

                }
            });
        }, 0)
    }

    function setDatepicker() {

        $(".datepicker_"+link_id).multiDatesPicker({
            changeMonth: true,
            changeYear: true,
            monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            beforeShow: addCheckboxDatepicker,
            onChangeMonthYear: addCheckboxDatepicker,
            dateFormat: "yy-m-d",
            onSelect: addCheckbox
        });

    }

    function addCheckbox(date, inst) {
        status  = date;
        $.ajax({url: '{{route('link.datemark.ajax')}}' + '?monitoring_id=' + monitoring_id + '&link_id=' + link_id+ '&status=' + status, success: function(result){
            console.log(result.msg);
            var arr = result.active_dates;
            var dates =  $(".datepicker_"+link_id).multiDatesPicker('getDates');
            $(".datepicker_"+link_id).multiDatesPicker('removeDates', dates );
            for (var i = 0; i < arr.length; i++) {
                $(".datepicker_"+link_id).multiDatesPicker('addDates', [new Date(arr[i])]);
            }
            check();
        }});
    }

    function mark(l_id, m_id) {
        link_id = l_id;
        monitoring_id = m_id;
        status = '';

        $.ajax({url: '{{route('link.datemark.ajax')}}' + '?monitoring_id=' + monitoring_id + '&link_id=' + link_id+ '&status=' + status, success: function(result){
            setDatepicker();
            var arr = result.active_dates;

            for (var i = 0; i < arr.length; i++) {
                console.log(arr[i]);
                $(".datepicker_"+link_id).multiDatesPicker('addDates', [new Date(arr[i])]);
            }
            check();
        }});

    }

    $(".datemark-btn").click(function () {
        var monitoring_id = $(this).data('monitoring_id');
        var link_id = $(this).data('link_id');
        mark(link_id, monitoring_id);
    });

    $(".modal_close").click(function () {
        location.reload();
    });



</script>

@stack('scripts')

<script type="text/javascript">

    $(".switch-checkbox").change(function () {
        var link_id = $(this).data('link_id');
        var monitoring_id = $(this).data('monitoring_id');
        var type = $(this).data('type');
        var check = 'off';
        if (this.checked) {
            check = 'on';
        }
        var this_is = $(this);
        $.ajax({url: '{{route('link.mark.ajax')}}' + '?link_id='+link_id+'&monitoring_id=' + monitoring_id + '&check='+ check, success: function(result){
            this_is.closest('.result-check').removeClass( "neutral" );
            reset_graph(monitoring_id, type)
        }});
    });

    $(".switch-neutral").change(function () {
        var link_id = $(this).data('link_id');
        var monitoring_id = $(this).data('monitoring_id');
        var type = $(this).data('type');

        var check = 'off';
        var parent = $(this).closest('.mark-area');
        if (this.checked) {
            check = 'neutral';
            parent.find('.result-check').addClass( "neutral" );
            parent.find('.datemark-btn').addClass( "hide" );
            parent.find('.switch-checkbox').attr("disabled", true);

            $(this).closest('.item-wrapper').find('.result-item').removeClass( "safe" );
            $(this).closest('.item-wrapper').find('.result-item').removeClass( "unsafe" );

        } else {
            parent.find('.result-check').removeClass( "neutral" );
            parent.find('.switch-checkbox').removeAttr('checked');
            parent.find('.switch-checkbox').removeClass('ez-checked');
            parent.find('.datemark-btn').removeClass( "hide" );
            parent.find('.switch-checkbox').removeAttr("disabled");
            $(this).closest('.item-wrapper').find('.result-item').addClass( "unsafe" );
        }

        $.ajax({url: '{{route('link.mark.ajax')}}' + '?link_id='+link_id+'&monitoring_id=' + monitoring_id + '&check='+ check, success: function(result){
            reset_graph(monitoring_id, type)
        }});
    });


    function reset_graph(m_id, type) {

        $.ajax({url: '{{route('graph.reset.ajax')}}' + '?monitoring_id=' + m_id +'&type=' + type, success: function(result){
            var arr = result ;
            var lastItem = arr[arr.length - 1];

            $('.percent-val').html(lastItem);

            chart.update({
                series: [{
                    data: arr
                }]
            });
        }});

    }
    /// neutral

    $(".reset-monitoring").click(function () {
        var monitoring_id = $(this).data('monitoring_id');

        $.ajax({url: '{{route('monitoring.reset')}}' + '?monitoring_id=' + monitoring_id, success: function(result){
            setTimeout(function () {location.reload();}, 1000);
        }});

    });

</script>

</body>
</html>