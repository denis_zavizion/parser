@extends('app')

@section('content')

    <a href="{{ route('monitoring.index') }}" class="settings-fixed-icon"><i class="fa fa-gears"></i></a>

    @include('monitoring/partials/_item',[ 'minitoring' => $monitoring, 'is_home' => false ])

@endsection

