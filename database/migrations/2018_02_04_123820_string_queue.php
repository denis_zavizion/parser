<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StringQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('string_queue', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('string_id')->unsigned();
            $table->json('json_string')->nullable();
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->integer('try_counter')->unsigned()->default(0);

            $table->index('user_id');
            $table->index('string_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('string_id')->references('id')->on('table_all_strings');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('string_queue', function (Blueprint $table) {
            $table->dropForeign('string_queue_user_id_foreign');
            $table->dropForeign('string_queue_string_id_foreign');
        });
        Schema::dropIfExists('string_queue');
    }
}
