<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('login')->unique();
            $table->string('password');
            $table->string('email');
            $table->rememberToken();
            $table->string('surname')->nullable();
            $table->string('name')->nullable();
            $table->string('cv')->nullable();
            $table->string('tel')->nullable();
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->boolean('role')->default(false);
            $table->boolean('current_status_pay')->nullable();
            $table->integer('unpaid')->nullable();
            $table->integer('pay')->nullable();
            $table->timestamp('pay_date')->nullable();
            $table->timestamp('next_pay_date')->nullable();
            $table->timestamps();

            // Indexes
            $table->index('login');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
