@extends('app')

@section('content')

    @foreach($items as $key => $item)
        <p>
            <h1>{{ $item['snippet'] }}</h1>
            <img src="{{ $item['link'] }}" alt="{{ $item['snippet'] }}" />
        <h2> {{ $item['time'] }}</h2> <h3>{{ $item['width'] }} X {{ $item['height'] }}</h3>
        </p>
    @endforeach

@endsection