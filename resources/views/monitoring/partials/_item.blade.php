<div class="index-content">
    <h1>{{ $monitoring->string_header }}</h1>
    <div class="info">
        <span class="item-msg-buttom"> {{ $monitoring->getInfo() }} </span>
    </div>
    <div class="main-item clearfix">

        <div class="percent-column">
            <div class="percent-item">
                <span class="percent-val">{{ $monitoring->controlPercent() }}</span><span class="percent-unit">%</span>

                <div class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></div>


                @if( !$is_home )
                    <span class="percent-updater reset-monitoring"
                          data-modal="#modal_updates"
                          data-monitoring_id="{{ $monitoring->id }}"
                            >
                            <i class="fa fa-refresh"></i>
                    </span>
                @else

                @endif

            </div>
        </div>
        @include('link/partials/graph',[ 'type' => 'all', 'minitoring' => $monitoring ])
    </div>
    @if($monitoring->is_active_today)
        <div class="info">
            <span class="@if($monitoring->is_active_today) active-today @endif">Время старта {{ $monitoring->date_scheduled[0]  }}</span>
        </div>
        @endif
                <!-- end main-item -->
        <div class="services-list">

            @if($monitoring->isOn('google'))
                <div class="service-item">
                    @php
                        $count = newMonitoringsCount($monitoring,'g');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.all', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif
                    <a href="{{ route('link.all', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('g') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/google-logo.png') }}"></span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>

                </div>
                <!-- end service-item -->
            @endif

            @if($monitoring->isOn('google_mobile'))
                <div class="service-item">
                    @php
                        $count = newMonitoringsCount($monitoring,'gm');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.mobile', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif
                    <a href="{{ route('link.mobile', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('gm') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/google-logo.png') }}"></span>
                                    <span class="service-subtitle">мобильный поиск</span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>
                </div>
                <!-- end service-item -->
            @endif

            @if($monitoring->isOn('google_news'))
                <div class="service-item">
                    @php
                        $count = newMonitoringsCount($monitoring,'gn');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.news', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif
                    <a href="{{ route('link.news', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('gn') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/google-logo.png') }}"></span>
                                    <span class="service-subtitle">новости</span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>
                </div>
                <!-- end service-item -->
            @endif

            @if($monitoring->isOn('google_image'))
                <div class="service-item">
                    @php
                        $count = newMonitoringsCount($monitoring,'gi');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.image', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif
                    <a href="{{ route('link.image', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('gi') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/google-logo.png') }}"></span>
                                    <span class="service-subtitle">картинки</span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>
                </div>
                <!-- end service-item -->
            @endif

            @if($monitoring->isOn('google_video'))
                <div class="service-item">
                    @php
                        $count = newMonitoringsCount($monitoring,'gv');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.video', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif
                    <a href="{{ route('link.video', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('gv') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/google-logo.png') }}"></span>
                                    <span class="service-subtitle">видео</span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>
                </div>
                <!-- end service-item -->
            @endif

            @if($monitoring->isOn('youtube'))
                <div class="service-item">

                    @php
                        $count = newMonitoringsCount($monitoring,'yo');
                    @endphp
                    @if($count>0)
                        <a href="{{ route('link.youtube', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="btn-new">
                            {{ $count }}
                        </a>
                    @endif

                    <a href="{{ route('link.youtube', ['id' => $monitoring->id, 'day' => dayNow()]) }}" class="item-inner">
                                <span class="percent-service">
                                    <span class="percent-val">{{ $monitoring->controlPercent('yo') }}</span><span class="percent-unit">%</span>
                                    <span class="percent-loader"><img src="{{ asset('/images/update.gif') }}"></span>
                                </span>
                                <span class="service-name">
                                    <span class="service-logo"><img src="{{ asset('/images/youtube-logo.png') }}"></span>
                                </span>
                                <span class="service-navicon">
                                    <span class="bar"></span>
                                </span>
                    </a>
                </div>
                <!-- end service-item -->
            @endif

        </div>
        <!-- end services-list -->
</div><!-- end index-content -->