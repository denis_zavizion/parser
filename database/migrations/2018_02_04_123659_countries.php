<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Countries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('domain_id')->unsigned();
            $table->string('country');

            $table->index('domain_id');

            $table
                ->foreign('domain_id')
                ->references('id')
                ->on('google_domains')
            ;
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropForeign('countries_domain_id_foreign');
        });
        Schema::dropIfExists('countries');
    }


}
