<div class="form-row">
    <label class="form-label"><span>ФИО или бренд</span></label>

    <div class="inp-box">
        {!! Form::text('string', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('string')) <p class="error">{{ $errors->first('string') }}</p> @endif
    </div>

</div>
<div class="form-row">
    <label class="form-label"><span>Поисковые системы</span></label>
    <div class="inp-box">
        <ul class="checks-list check-logos">
            <li>
                <input name='parser_status[]' type="checkbox" id="check-gp" value="google"
                       @if (in_array("google", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for="check-gp">
                    <img src="{{ asset('/images/google-logo.png') }}">
                </label>
            </li>
            <li>
                <input name='parser_status[]' type="checkbox" id="check-gp-m" value="google_mobile"
                       @if (in_array("google_mobile", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for="check-gp-m">
                    <img src="{{ asset('/images/google-logo.png') }}">
                    <span class="subtitle">мобильный поиск</span>
                </label>
            </li>
            <li>
                <input name='parser_status[]'  type="checkbox" id="check-gp-n" value="google_news"
                       @if (in_array("google_news", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for id="check-gp-n">
                    <img src="{{ asset('/images/google-logo.png') }}">
                    <span class="subtitle">новости</span>
                </label>
            </li>
            <li>
                <input name='parser_status[]' type="checkbox" id="check-gp-k" value="google_image"
                       @if (in_array("google_image", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for="check-gp-k">
                    <img src="{{ asset('/images/google-logo.png') }}">
                    <span class="subtitle">картинки</span>
                </label>
            </li>
            <li>
                <input name='parser_status[]' type="checkbox" id="check-gp-v" value="google_video"
                       @if (in_array("google_video", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for="check-gp-v">
                    <img src="{{ asset('/images/google-logo.png') }}">
                    <span class="subtitle">видео</span>
                </label>
            </li>
            <li>
                <input name='parser_status[]' type="checkbox" id="check-yt" value="youtube"
                       @if (in_array("youtube", $monitoring->parser_status)) checked @endif>
                <label class="check-label" for="check-yt">
                    <img src="{{ asset('/images/youtube-logo.png') }}">
                </label>
            </li>
        </ul>
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Страна</span></label>
    <div class="inp-box">
        {{ Form::select('country_id', $countries, null, ['id' => 'input-country','class' => 'input-text']) }}
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Через прокси</span></label>
    <div class="inp-box">
        {{ Form::select('preset_id', $presets, null, ['id' => 'input-preset', 'class' => 'input-text']) }}
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Язык результата</span></label>
    <div class="inp-box">
        {{ Form::select('language_id', $languages, null, ['class' => 'input-text']) }}
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Глубина контроля</span></label>
    <div class="inp-box">
        {!! Form::number('pages_count', null, ['min'=>1,'max'=>1000,'class' => 'input-text input-small']) !!}
        <span class="inp-units">результатов</span>
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Время старта</span></label>
    <div class="inp-box">
        {!! Form::text('date_scheduled[0]', null, ['class' => 'input-text timepicker']) !!}
    </div>
</div>
<div class="form-row">
    <label class="form-label"></label>
    <div class="inp-box">
        <ul class="checks-list">
            <li>
                <input name='date_scheduled[1][]' value="Monday" type="checkbox" id="week-mn"
                       @if (in_array("Monday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-mn">ПН</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Tuesday" type="checkbox" id="week-ts"
                       @if (in_array("Tuesday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-ts">ВТ</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Wednesday" type="checkbox" id="week-wd"
                       @if (in_array("Wednesday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-wd">СР</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Thursday" type="checkbox" id="week-th"
                       @if (in_array("Thursday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-th">ЧТ</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Friday" type="checkbox" id="week-fr"
                       @if (in_array("Friday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-fr">ПТ</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Saturday" type="checkbox" id="week-st"
                       @if (in_array("Saturday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-st">СБ</label>
            </li>
            <li>
                <input name='date_scheduled[1][]' value="Sunday" type="checkbox" id="week-sn"
                       @if (in_array("Sunday", $monitoring->date_scheduled[1])) checked @endif>
                <label class="check-label" for="week-sn">ВС</label>
            </li>
        </ul>
    </div>
</div>
<div class="submit-row">
    <div class="inp-box"><input type="submit" class="btn" value="Сохранить"></div>
</div>

@push('scripts')

    <script type="text/javascript">
        $("#input-country").change(function() {
            var val = $(this).val();
            $("#input-preset").val(val);
        });
    </script>

@endpush
