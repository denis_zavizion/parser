<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Aparser\Aparser;

class Country extends Model {

    protected $table = 'countries';

    public $timestamps = false;

    protected $fillable = [
        'preset',
        'en'
    ];

    public function domain()
    {
        return $this->belongsTo('App\Models\Domain');
    }

    public function getProxyPresetAttribute()
    {
        if (is_null($this->preset)) {
            return str_slug($this->en, '_');
        }

        return $this->preset;
    }

    public function getCountryProxyAttribute()
    {
        return $this->country.' ('.$this->proxy_preset.')';
    }

}
