<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('test')->middleware('login')->get('/test', 'TestController@go');
Route::name('webhook')->post('/webhook', 'TelegramController@webhook');

Route::name('index')->middleware('login')->get('/', 'HomeController@index');
Route::name('home')->middleware('login')->get('/home', 'HomeController@index');

Route::name('login')->get('/login', 'UserController@showLoginForm');
$this->post('login', 'UserController@login');

Route::name('logout')->middleware('login')->get('/logout', function () {
    Auth::logout();
    return Redirect::route('home');
});

Route::name('user.token.clear')->middleware('login')->get('/user/cleartoken/{id}', 'UserController@cleartoken');
Route::name('user.new')->middleware('login', 'admin')->get('/user/new', 'UserController@create');
Route::name('user.new.post')->middleware('login', 'admin' )->post('/user/new', 'UserController@create');
Route::name('user.edit')->middleware('login')->get('/user/edit/{id}', 'UserController@edit');
Route::name('user.edit.post')->middleware('login')->post('/user/edit/{id}', 'UserController@edit');
Route::name('user.index')->middleware('login', 'admin')->get('/users', 'UserController@index');
Route::name('user.delete')->middleware('login', 'admin')->get('/user/delete/{id}', 'UserController@delete');

Route::name('monitoring.new')->middleware('login')->get('/monitoring/new', 'MonitoringController@create');
Route::name('monitoring.new.post')->middleware('login')->post('/monitoring/new', 'MonitoringController@create');
Route::name('monitoring.edit')->middleware('login')->get('/monitoring/edit/{id}', 'MonitoringController@edit');

Route::name('monitoring.edit.post')->middleware('login')->post('/monitoring/edit/{id}', 'MonitoringController@edit');
Route::name('monitoring.delete')->middleware('login')->get('/monitoring/delete/{id}', 'MonitoringController@delete');

Route::name('monitoring.view')->middleware('login')->get('/monitoring/view/{id}', 'MonitoringController@view');

Route::name('monitoring.index')->middleware('login')->get('/monitorings', 'MonitoringController@index');

Route::name('country.index')->middleware('login', 'admin')->get('/countries', 'CountryController@index');


Route::name('link.all')->middleware('login')->get('/Link/all/{id}/{day}/{mark?}', 'LinkController@all');
Route::name('link.mobile')->middleware('login')->get('/Link/mobile/{id}/{day}/{mark?}', 'LinkController@mobile');
Route::name('link.news')->middleware('login')->get('/Link/news/{id}/{day}/{mark?}', 'LinkController@news');
Route::name('link.image')->middleware('login')->get('/Link/image/{id}/{day}/{mark?}', 'LinkController@image');
Route::name('link.video')->middleware('login')->get('/Link/video/{id}/{day}/{mark?}', 'LinkController@video');
Route::name('link.youtube')->middleware('login')->get('/Link/youtube/{id}/{day}/{mark?}', 'LinkController@youtube');

Route::name('se.se_search')->middleware('login')->get('/se_search', function () {
    return view('se.se_search');
});

Route::name('se.se_mobile')->middleware('login')->get('/se_mobile', function () {
    return view('se.se_mobile');
});

Route::name('se.se_image')->middleware('login')->get('/se_image', function () {
    return view('se.se_image');
});

Route::name('se.se_news')->middleware('login')->get('/se_news', function () {
    return view('se.se_news');
});

Route::name('se.se_video')->middleware('login')->get('/se_video', function () {
    return view('se.se_video');
});

Route::name('se.se_youtube')->middleware('login')->get('/se_youtube', function () {
    return view('se.se_youtube');
});

/// ajax
Route::name('link.mark.ajax')->middleware('login', 'ajax')->get('/link/mark/ajax', 'LinkController@ajaxMark');
Route::name('link.datemark.ajax')->middleware('login', 'ajax')->get('/link/datemark/ajax', 'LinkController@ajaxDateMark');
Route::name('monitoring.reset')->middleware('login', 'ajax')->get('/monitoring/reset', 'MonitoringController@ajaxReset');
Route::name('graph.reset.ajax')->middleware('login', 'ajax')->get('/monitoring/reset/graph', 'MonitoringController@ajaxResetGraph');

Route::name('user.role.ajax')->middleware('login', 'ajax', 'admin')->get('/user/role/ajax', 'UserController@ajaxRole');
Route::name('user.status.reset')->middleware('login', 'ajax', 'admin')->get('/user/status/ajax', 'UserController@ajaxStatus');
Route::name('monitoring.status.reset')->middleware('login', 'ajax')->get('/monitoring/status/ajax', 'MonitoringController@ajaxStatus');
Route::name('country.preset.reset')->middleware('login', 'ajax', 'admin')->get('/country/preset/ajax', 'CountryController@ajaxPresetReset');