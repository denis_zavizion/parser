<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Aparser\Aparser;
use App\User;
use Illuminate\Support\Facades\DB;
use Mail;

class Queue extends Model {

    const  STATUS_WAITING = 0;
    const  STATUS_QUEUE = 1;
    const  STATUS_IN_PROGRESS = 2;
    const  USEPROXY = 1;
    const  DEFAULT_PROXY = "default_proxy";
    

    protected $table = 'string_queue';

    protected $aparser;

    protected $fillable = [
        'user_id',
        'string_id',
        'json_string',
        'status',
        'try_counter',
    ];

    protected $casts = [
        'json_string' => 'array',
    ];

    /*
    |--------------------------------------------------------------------------
    | Static Methods
    |--------------------------------------------------------------------------
    */

    /**
     * парсер  берёт задания и парсит его (в начале 10 попыток с задержкой) в конце
     * успешного парсинга удаляем задачу из очереди задач и если оно последнее для мониторига то создаем статистику за день
     * когда парсер неуспешный то ставим задание в конец очереди со статусом waiting
     */
    public static function parseFirstPack()
    {
        $pack_size = env('APARSER_QUEUE_PACK_SIZE');

        while ($pack_size > 0) {

            $first_in_pack= Queue::where('status', self::STATUS_QUEUE)
                ->orderBy('created_at', 'asc')
                ->first();

            if(!$first_in_pack) {
                $first_in_pack= Queue::where('status', self::STATUS_WAITING)
                    ->orderBy('created_at', 'asc')
                    ->first();
            }

            if ($first_in_pack) {
                $type = $first_in_pack->json_string['type'];

                /// инициализируем парсер
                $first_in_pack->initAparser();

                /// "google", "google_mobile", "google_news", "google_image", "google_video", "youtube"
                /// проверить парсера на доступность
                if($first_in_pack->aparserPing()) {

                    $first_in_pack->inProgressNow();

                    if ($type == 'google') {
                        $first_in_pack->parseGoogle();

                    } else if ($type == 'google_mobile') {
                        $first_in_pack->parseGoogleMobile();

                    } else if ($type == 'google_news') {
                        $first_in_pack->parseNews();

                    } else if ($type == 'google_image') {
                        $first_in_pack->parseGoogleImage();

                    } else if ($type == 'google_video') {
                        $first_in_pack->parseVideo();

                    } else if ($type == 'youtube') {
                        $first_in_pack->parseYoutube();

                    }
                    /// cтроим статистику для пропарсенных данных
                    $first_in_pack->monitoring->buildStatistics(dayNow());

                    $first_in_pack->delete();

                } else {
                    $first_in_pack->waitingNow();
                }
            }

            $pack_size --;
        }

    }

    /*
	|--------------------------------------------------------------------------
	| Relationship Methods
	|--------------------------------------------------------------------------
	*/

    public function monitoring()
    {
        return $this->belongsTo('App\Models\Monitoring', 'string_id', 'id' );
    }

    /*
	|--------------------------------------------------------------------------
	| Methods
	|--------------------------------------------------------------------------
	*/

    public function aparserPing() {

        $isOkPing = false;

        $ping_count = 10;

        while (!$isOkPing && $ping_count  > 0) {

            try{
                $isOkPing = $this->aparser->ping() == 'pong';
            }
            catch(\Exception $e){
                sleep(10);
            }

            $ping_count --;
        }

        return $isOkPing;

    }

    public function initAparser() {

        $this->aparser = new Aparser(env('APARSER_IP'), env('APARSER_PASSWORD'), ['go'=>'true'], $this);

    }

    public function inProgressNow()
    {
        $this->update([
            'status' => self::STATUS_IN_PROGRESS,
        ]);
    }

    public function waitingNow()
    {
        $this->update([
            'status' => self::STATUS_WAITING,
        ]);
    }

    public function parseGoogle() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];
        $options = [
            [
                "value" => linkToPage($links_count),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" => $this->json_string['domain'],
                "type" => "override",
                "id" => "domain",
            ],
            [
                "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
            [
                "value" => $this->json_string['language'],
                "type" => "override",
                "id" => "lr",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);

        }
        catch(\Exception $e){

            $msg = 'При парсинге прокси чекером - '.$proxy_preset.' произошла ошибка. Парсинг переключен на - '.self::DEFAULT_PROXY;

            Mail::raw($msg, function ($message) {
                $message->from(\Config::get('mail.from.address'), \Config::get('mail.from.address'));
                
                
                $message->to(User::adminEmail(), 'Jon Doe')->subject('Ошибка парсинга!');
            });

            ///  парсинг аварийным проксичекером 
            $options = [
                [
                    "value" => linkToPage($links_count),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => 'override',
                    "id" => "useproxy",
                ],

                [
                    "value" => $this->json_string['domain'],
                    "type" => "override",
                    "id" => "domain",
                ],
                [
                    "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
                [
                    "value" => $this->json_string['language'],
                    "type" => "override",
                    "id" => "lr",
                ],
            ];
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);
        }

        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 3) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [
                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'g'),
                    'parsed_by' => 'g', /// g gm gn gi gv yo
                    'anchor' =>  str_limit($arr_line[1], $limit = 255, $end = ''),
                    'snippet' => $arr_line[2],
                    //'serp_time' => $arr_line[0],
                    //'username' => $arr_line[0],// yo
                    //'views' => $arr_line[0],// yo
                    //'timing' => $arr_line[0],// yo
                    //'preview' => $arr_line[0],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }
        if (count($items) == 0) {
            \Log::error('g');
        } else {
            $this->resetToday(dayNow(), 'g');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }

    public function parseVideo() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];
        $options = [
            [
                "value" => linkToPage($links_count),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" => $this->json_string['domain'],
                "type" => "override",
                "id" => "domain",
            ],
            [
                "value" => "vid",
                "type" => "override",
                "id" => "serp",
            ],
            [
                "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
            [
                "value" => $this->json_string['language'],
                "type" => "override",
                "id" => "lr",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);

        }
        catch(\Exception $e){
            $options = [
                [
                    "value" => linkToPage($links_count),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => "override",
                    "id" => "useproxy",
                ],
                [
                    "value" => $this->json_string['domain'],
                    "type" => "override",
                    "id" => "domain",
                ],
                [
                    "value" => "vid",
                    "type" => "override",
                    "id" => "serp",
                ],
                [
                    "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
                [
                    "value" => $this->json_string['language'],
                    "type" => "override",
                    "id" => "lr",
                ],
            ];
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);

        }

        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 3) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [
                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'gv'),
                    'parsed_by' => 'gv', /// g gm gn gi gv yo
                    'anchor' => str_limit($arr_line[1], $limit = 255, $end = ''),
                    'snippet' => $arr_line[2],
                    //'serp_time' => $arr_line[0],
                    //'username' => $arr_line[0],// yo
                    //'views' => $arr_line[0],// yo
                    //'timing' => $arr_line[0],// yo
                    //'preview' => $arr_line[0],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }
        if (count($items) == 0) {
            \Log::error('gv');
        } else {
            $this->resetToday(dayNow(), 'gv');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }

    public function parseNews() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];
        $options = [
            [
                "value" => linkToPage($links_count),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" => $this->json_string['domain'],
                "type" => "override",
                "id" => "domain",
            ],
            [
                "value" => "nws",
                "type" => "override",
                "id" => "serp",
            ],
            [
                "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
            [
                "value" => $this->json_string['language'],
                "type" => "override",
                "id" => "lr",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);
   
        }
        catch(\Exception $e){
            $options = [
                [
                    "value" => linkToPage($links_count),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => "override",
                    "id" => "useproxy",
                ],
                [
                    "value" => $this->json_string['domain'],
                    "type" => "override",
                    "id" => "domain",
                ],
                [
                    "value" => "nws",
                    "type" => "override",
                    "id" => "serp",
                ],
                [
                    "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
                [
                    "value" => $this->json_string['language'],
                    "type" => "override",
                    "id" => "lr",
                ],
            ];
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Modern', 'default',  0, $options);
   
        }
        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 3) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [
                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'gn'),
                    'parsed_by' => 'gn', /// g gm gn gi gv yo
                    'anchor' => str_limit($arr_line[1], $limit = 255, $end = ''),
                    'snippet' => $arr_line[2],
                    //'serp_time' => $arr_line[0],
                    //'username' => $arr_line[0],// yo
                    //'views' => $arr_line[0],// yo
                    //'timing' => $arr_line[0],// yo
                    //'preview' => $arr_line[0],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }
        if (count($items) == 0) {
            \Log::error('gn');
        } else {
            $this->resetToday(dayNow(), 'gn');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }

    public function parseGoogleMobile() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];
        $options = [
            [
                "value" => linkToPage($links_count),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
            [
                "value" => $this->json_string['language'],
                "type" => "override",
                "id" => "lr",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Mobile', 'default',  0, $options);
  
        }
        catch(\Exception $e){
            $options = [
                [
                    "value" => linkToPage($links_count),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => "override",
                    "id" => "useproxy",
                ],
                [
                    "value" => "\$serp.format('\$link***\$anchor***\$snippet###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
                [
                    "value" => $this->json_string['language'],
                    "type" => "override",
                    "id" => "lr",
                ],
            ];
    
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Mobile', 'default',  0, $options);
  
        }
        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 3) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [
                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'gm'),
                    'parsed_by' => 'gm', /// g gm gn gi gv yo
                    'anchor' => str_limit($arr_line[1], $limit = 255, $end = ''),
                    'snippet' => $arr_line[2],
                    //'serp_time' => $arr_line[0],
                    //'username' => $arr_line[0],// yo
                    //'views' => $arr_line[0],// yo
                    //'timing' => $arr_line[0],// yo
                    //'preview' => $arr_line[0],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }

        if (count($items) == 0) {
            \Log::error('gm');
        } else {
            $this->resetToday(dayNow(), 'gm');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }


    public function parseGoogleImage() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];

        $options = [
            [
                "value" => linkToPage($links_count),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" => "\$serp.format('\$link***\$width***\$height***\$snippet###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $this->json_string['domain'],
                "type" => "override",
                "id" => "domain",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
            [
                "value" => $this->json_string['language'],
                "type" => "override",
                "id" => "lr",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Images', 'default',  0, $options);

        }
        catch(\Exception $e){

            $options = [
                [
                    "value" => linkToPage($links_count),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => "override",
                    "id" => "useproxy",
                ],
                [
                    "value" => "\$serp.format('\$link***\$width***\$height***\$snippet###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => $this->json_string['domain'],
                    "type" => "override",
                    "id" => "domain",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
                [
                    "value" => $this->json_string['language'],
                    "type" => "override",
                    "id" => "lr",
                ],
            ];
    
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::Google::Images', 'default',  0, $options);

        }
        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 4) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [
                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'gi'),
                    'parsed_by' => 'gi', /// g gm gn gi gv yo
                    'anchor' => str_limit($arr_line[1].'x'.$arr_line[2], $limit = 255, $end = ''),
                    'snippet' => $arr_line[3],
                    //'serp_time' => $arr_line[0],
                    //'username' => $arr_line[0],// yo
                    //'views' => $arr_line[0],// yo
                    //'timing' => $arr_line[0],// yo
                    //'preview' => $arr_line[0],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }
        if (count($items) == 0) {
            \Log::error('gi');
        } else {
            $this->resetToday(dayNow(), 'gi');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }

    public function parseYoutube() {

        $links_count = $this->json_string['links_count'];
        $proxy_preset = $this->json_string['proxy_preset'];
        $options = [
            [
                "value" => linkToPage($links_count, 20),
                "type" => "override",
                "id" => "pagecount",
            ],
            [
                "value" => self::USEPROXY,
                "type" => "override",
                "id" => "useproxy",
            ],
            [
                "value" =>  "\$serp.format('\$link***\$title***\$desc***\$user***\$thumb***\$views***\$time###')",
                "type" => "override",
                "id" => "formatresult",
            ],
            [
                "value" => $proxy_preset,
                "type" => "override",
                "id" => "proxyChecker",
            ],
        ];

        try{
        
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::YouTube', 'default',  0, $options);
        }
        catch(\Exception $e){

            $options = [
                [
                    "value" => linkToPage($links_count, 20),
                    "type" => "override",
                    "id" => "pagecount",
                ],
                [
                    "value" => self::USEPROXY,
                    "type" => "override",
                    "id" => "useproxy",
                ],
                [
                    "value" =>  "\$serp.format('\$link***\$title***\$desc***\$user***\$thumb***\$views***\$time###')",
                    "type" => "override",
                    "id" => "formatresult",
                ],
                [
                    "value" => self::DEFAULT_PROXY,
                    "type" => "override",
                    "id" => "proxyChecker",
                ],
            ];
    
            $response =  $this->aparser->oneRequest($this->json_string['query'], 'SE::YouTube', 'default',  0, $options);
      
        }
        $strings = explode('###', $response['resultString']);

        $items = [];

        $date_now = dayNow();

        $position = 1;
        $link_arr = [];
        foreach ($strings as $string) {
            $arr_line = explode('***', $string);
            if (count($arr_line) == 7) {
                $link = str_limit($arr_line[0], $limit = 757, $end = '');
                if (in_array($link, $link_arr)) {
                    continue;
                }
                $link_arr[] = $link;
                $items[] = [

                    //'link' => $arr_line[0],
                    //'title' => $arr_line[1],
                    //'desc' => $arr_line[2],
                    //'user' => $arr_line[3],
                    //'thumb' => $arr_line[4],
                    //'views' => $arr_line[5],
                    //'time' => $arr_line[6],

                    'parsing_date' => $date_now,
                    'link' => $link,
                    'mark' => $this->setMarks($link, 'yo'),
                    'parsed_by' => 'yo', /// g gm gn gi gv yo
                    'anchor' => $arr_line[1],
                    'snippet' => $arr_line[2],
                    //'serp_time' => $arr_line[0],
                    'username' => $arr_line[3],// yo
                    'views' => $arr_line[5]=='none' ?  0 : $arr_line[5],// yo
                    'timing' => $arr_line[6],// yo
                    'preview' => $arr_line[4],// yo
                    'position' => $position,
                    'domain' => domain($arr_line[0]),
                ];
            }

            if ($position > $links_count) {
                break;
            };
            $position++;
        }
        
        if (count($items) == 0) {
            \Log::error('yo');
        } else {
            $this->resetToday(dayNow(), 'yo');

            DB::table('string_'.$this->string_id)->insert($items);
        }

    }

    public function arhiveRequest($action, $request_json) {

        if ($action == 'oneRequest'){
            DB::table('string_go')->insert(
                [
                    'user_id' => $this->user_id,
                    'string_id' => $this->string_id,
                    'json_string' => $request_json,
                    'last_success_request_date' => timeNow(),
                    'next_request_date' => timeTomorrow(),
                ]);
        }

    }

    /// в случае повторного парсинга  в этот же день данные за этот день удаляются чтобы не было дублей
    public function resetToday($day, $parsed_by) {
        DB::table('string_'.$this->string_id)
            ->where('parsed_by', $parsed_by)
            ->whereDate('parsing_date', '=', $day)
            ->delete()
        ;
    }

    public function setMarks($link, $parsed_by) {
        $arr = $this->oldMarks($parsed_by);

        $mark = 0;
        if ( isset($arr[$link])) {
            $mark = $arr[$link];
        }
        return $mark;
    }

    public function oldMarks($parsed_by) {

        return DB::table('string_'.$this->string_id)
            ->where('parsed_by', $parsed_by)
            ->where('mark', '<>', 0)
            ->whereDate('parsing_date', '<>', dayNow())
            ->orderBy('parsing_date', 'asc')
            ->pluck('mark', 'link')
        ;
    }

}
