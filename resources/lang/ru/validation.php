<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute должен быть принят ',
    'active_url'           => ' :attribute не является допустимым URL-адресом.',
    'after'                => ' :attribute должен быть датой после :date.',
    'alpha'                => ' :attribute может содержать только буквы.',
    'alpha_dash'           => ' :attribute может содержать только буквы, цифры и дефисы ',
    'alpha_num'            => ' :attribute может содержать только буквы и цифры.',
    'array'                => ' :attribute должен быть массивом.',
    'before'               => ' :attribute должна быть датой до :date.',
    'between'              => [
        'numeric' => ' :attribute должен быть между :min и :max значениями.',
        'file'    => ' :attribute должен быть между :min и :max килобайтами.',
        'string'  => ' :attribute должен быть между :min и :max значениями.',
        'array'   => ' :attribute должен быть между :min и :max значениями.',
    ],
    'boolean'              => ' Поле :attribute должно быть истинным или ложным.',
    'confirmed'            => ' Подтверждение для поля :attribute не совпадает.',
    'date'                 => ' Недопустимый :attribute.',
    'date_format'          => ' :attribute не соответствует формату :format.',
    'different'            => ' :attribute и другие должны быть разными.',
    'digits'               => ' :attribute должен быть :digits разрядов.',
    'digits_between'       => ' :attribute должен быть между :min и :max разрядами.',
    'email'                => ' :attribute должен быть существующим адресом электронной почты.',
    'filled'               => ' :attribute обязательное поле.',
    'exists'               => ' Выбранный :attribute атрибут недействителен.',
    'image'                => ' :attribute должен быть изображением.',
    'in'                   => ' Выбранный :attribute атрибут недействителен.',
    'integer'              => ' :attribute должен быть целым числом.',
    'ip'                   => ' :attribute должен быть существующим IP-адресом.',
    'max'                  => [
        'numeric' => ' :attribute не может быть больше :max.',
        'file'    => ' :attribute не может быть больше :max килобайт.',
        'string'  => ' :attribute не может быть больше :max символов.',
        'array'   => ' :attribute не может содержать больше :max элементов.',
    ],
    'mimes'                => ' :attribute должен быть файлом типа :values.',
    'min'                  => [
        'numeric' => ' :attribute должен быть не менее :min.',
        'file'    => ' :attribute должен быть как минимум :min килобайт.',
        'string'  => ' :attribute должен быть как минимум :min символов.',
        'array'   => ' :attribute должен иметь как минимум :min элементов.',
    ],
    'not_in'               => ' Выбранный :attribute недействительный.',
    'numeric'              => ' :attribute должен быть числом.',
    'regex'                => ' :attribute формат недействительный.',
    'required'             => ' :attribute обязательное поле.',
    'required_if'          => ' Поле :attribute требуется, если :other является :value.',
    'required_with'        => ' Поле :attribute требуется, когда  значение :values присутствует.',
    'required_with_all'    => ' Поле :атрибут требуется, когда  все значения :values присутствуют.',
    'required_without'     => ' Поле :attribute необходимо, если :values не присутствует.',
    'required_without_all' => ' Поле :attribute требуется, если  все значения :values не присутствуют.',
    'same'                 => ' Параметр :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => ' :attribute должен быть :size.',
        'file'    => ' :attribute должен быть :size килобайт.',
        'string'  => ' :attribute должен содержать :size символов.',
        'array'   => ' :attribute должен содержать :size элементов.',
    ],
    'unique'               => ':attribute уже занят.',
    'url'                  => ' :attribute неправильный формат.',
    'timezone'             => ' :attribute должен быть  в допустимой  временной зоне.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'first_ip' => [
            'unique' => ' Первая и последняя комбинация IP-адресов уже существует.',
        ],
        'last_ip' => [
            'unique' => ' Первая и последняя комбинация IP-адресов уже существует.',
        ],
    ],
    'abuseclass'            => ' Поле :attribute должно быть известно :attribute',
    'abusetype'             => ' Поле :attribute должно быть известно :attribute',
    'json'                  => ' Поле :attribute не трансформировалось в действительный объект JSON ',
    'uri'                   => ' Поле :attribute должно быть допустимым URI или Path ',
    'uniqueflag'            => ' Флажок :attribute уже установлен на другом объекте ',
    'domain'                => ' Поле :attribute требует действительного имени домена ',
    'string'                => ' Поле :attribute должно быть строкой ',
    'stringorboolean'       => ' Поле :attribute должно быть строкой или логическим выражением',
    'file'                  => ' Поле :attribute должно содержать действительное местоположение файла ',
    'emails'                => ' Все :attribute атрибутов должны быть действительными.',
    'timestamp'             => ' Поле :attribute содержать действительное целочисленное timestamp значение.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'title' => 'Название организации',
        'mobile' => 'Номер телефона',
        'email' => 'Email',
        'address' => 'Адрес',
        'city_id' => 'Город',
        'name' => 'Имя',
        'text' => 'Это',
        'password' => 'Пароль',
        'login' => 'Логин',
        'pages_count' => 'Глубина контроля',
        'start_time' => 'Время старта',
        'string' => 'ФИО или бренд',

    ],

];