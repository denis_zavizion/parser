@extends('app')

@section('content')
    <a href="{{ route('monitoring.index') }}" class="settings-fixed-icon"><i class="fa fa-gears"></i></a>
    <div class="se-content">
        <div class="heading clearfix">
            <div class="heading-logo"><img src={{ asset('/images/google-logo.png') }}><span class="subtitle">Поиск</span></div>
            <h1><a href="{{ route('monitoring.view', $monitoring->id) }}">{{ $monitoring->string_header }}</a></h1>
        </div>
        <div class="main-item clearfix">
            <div class="percent-column">
                <div class="percent-item">
                    <span class="percent-val">{{ $monitoring->controlPercent('g') }}</span><span class="percent-unit">%</span>
                </div>
            </div>
            @include('link/partials/graph',['type' => 'g', 'minitoring' => $monitoring ])
        </div><!-- end main-item -->
        @if($mark)<div class="main-item clearfix">{{ count($links) }} из {{ $all_count }} </div>@endif
        <div class="se-results">
            <table class="data-table results-table">
                <thead>
                    <tr>
                        <th>Статус</th>
                        <th>Контент</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($links as $key => $link)
                        <tr>
                            <td class="cell-actions">
                                @include('link/partials/mark',['type' => 'g', 'minitoring' => $monitoring ])
                            </td>
                            <td>
                                <div class="news-item result-item no-thumb safe">
                                    <div class="item-inner clearfix">
                                        <div class="entry">
                                            <h4><a href="{{ $link->link }}">{!! printEm($link->anchor) !!}</a></h4>
                                            <div class="item-meta"><a href="{{ $link->link }}">{{ $link->link }}</a></div>
                                            <div class="excerpt"> {!! printEm($link->snippet) !!}</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="modal_result_{{ $key }}" class="modal hide fade" tabindex="-{{ $key }}">
                                    <span class="modal_close" data-dismiss="modal" aria-hidden="true"></span>
                                    <div class="modal_inner">
                                        <div class="updates-list">
                                            <div class="news-item update-item no-thumb safe">
                                                <div class="item-inner clearfix">
                                                    <div class="entry">
                                                        <h4><a href="{{ $link->link }}">{!! printEm($link->anchor) !!} </a></h4>
                                                        <div class="item-meta"><a href="{{ $link->link }}">{{ $link->link }}</a></div>
                                                        <div class="excerpt"> {!! printEm($link->snippet) !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
@include('link/partials/datemark',['type' => 'yo', 'minitoring' => $monitoring, 'link' => $link ])
                                    </div>
                                </div><!-- end modal_result -->
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            @if(!$mark)
                {!! $links->render() !!}
            @else
                <div class="more-wrap">
                    <a href="{{ route('link.all', ['id' => $monitoring->id, 'day' => dayNow(), 'mark' => 'new']) }}" class="more-load">
                        <span>Следующие</span>
                    </a>
                </div>
            @endif

        </div>
    </div><!-- end se-content -->
@endsection