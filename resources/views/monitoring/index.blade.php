@extends('app')

@section('content')
    @include('menu', ['page' => 'monitoring.index'])
    <div id="container">
        <div class="top-actions">
            <a href="{{ route('monitoring.new') }}" class="btn btn-second"><i class="fa fa-calendar-plus-o"></i> Добавить мониторинг</a>
        </div>
        <table class="data-table settings-table">
            <thead>
                <tr>
                    <th class="a-left">Запрос</th>
                    <th class="nowrap">Время парсинга</th>
                    <th class="nowrap">Статус</th>
                    <th class="nowrap">Общий контроль</th>
                    <th><img src={{ asset('/images/gp-ico.png') }}></th>
                    <th><img src={{ asset('/images/gp-mobile-ico.png') }}></th>
                    <th><img src={{ asset('/images/gp-news-ico.png') }}></th>
                    <th><img src={{ asset('/images/gp-images-ico.png') }}></th>
                    <th><img src={{ asset('/images/gp-video-ico.png') }}></th>
                    <th><img src={{ asset('/images/yt-ico.png') }}></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                @foreach($monitorings as $key => $monitoring)
                    <tr>
                        <td>
                            <button class="btn-clipboard" data-clipboard-text="{{ route('monitoring.view', $monitoring->id) }}">
                                <i class="fa fa-link"></i>
                            </button>
                            <a href="{{ route('monitoring.view', $monitoring->id) }}">{{ $monitoring->string_header }}
                                    <div class="index-msg-buttom"> {{ $monitoring->getInfo()}}</div>
                            </a>
                        </td>
                        <td class="percent-cell" data-th="Время старта">
                            <span class="@if($monitoring->is_active_today) active-today @endif"> {{ $monitoring->date_scheduled[0]  }}</span>
                        </td>
                        <td data-th="Статус">

                            @if($monitoring->isActive())
                                <a href="#" class="change-circle ajax-status"
                                   data-values="active,inactive"
                                   data-monitoring_id="{{ $monitoring->id }}"
                                        >active</a>
                            @else
                                <a href="#" class="change-circle ajax-status"
                                   data-values="active,inactive"
                                   data-monitoring_id="{{ $monitoring->id }}"
                                        >inactive</a>
                            @endif

                        </td>
                        <td class="percent-cell" data-th="Общий контроль">{{ $monitoring->controlPercent() }}%</td>
                        <td class="percent-cell" data-th="G">{{ $monitoring->controlPercent('g') }}%</td>
                        <td class="percent-cell" data-th="Gм">{{ $monitoring->controlPercent('gm') }}%</td>
                        <td class="percent-cell" data-th="Gн">{{ $monitoring->controlPercent('gn') }}%</td>
                        <td class="percent-cell" data-th="Gк">{{ $monitoring->controlPercent('gi') }}%</td>
                        <td class="percent-cell" data-th="Gв">{{ $monitoring->controlPercent('gv') }}%</td>
                        <td class="percent-cell" data-th="Youtube">{{ $monitoring->controlPercent('yo') }}%</td>
                        <td class="table-actions">
                            <a onclick="return confirm('Вы уверены?')" href="{{ route('monitoring.delete', $monitoring->id) }}" class="remove-link"></a>
                            <a href="{{ route('monitoring.edit', $monitoring->id) }}" class="add-link">
                                <i class="fa fa-gears"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $monitorings->render() !!}
    </div>
@endsection

@push('scripts')

<script type="text/javascript">

    $(".ajax-status").click(function () {
        var monitoring_id = $(this).data('monitoring_id');
        var val =  $(this).html();
        $.ajax({url: '{{route('monitoring.status.reset')}}' + '?monitoring_id=' + monitoring_id + '&val='+ val, success: function(result){

        }});
    });

</script>

@endpush