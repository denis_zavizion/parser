<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Telegram\Bot\Api;

class TelegramMessage {

	public static function sendGtoken($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();


		if ($telegramAccount) {
			if ($telegramAccount->is_guest) {
				$response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
					'text' => 'Команда доступна только пользователю с токеном владельца.', 
				]);
			} else {
				$response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
					'text' => $telegramAccount->user->createGuestAccount(), 
				]);
			}
		}
        
	}

	public static function sendAuto($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();


		if ($telegramAccount) {

			$telegramAccount->previous_command = '/auto_auto_period';
			$telegramAccount->save();

			$response = $telegram->sendMessage([
				'chat_id' => $chat_id,
				'parse_mode' => 'html',
				'text' => '<b>С какой частотой присылать отчет?</b>
Введите цифру от 1. Например, если вы хотите получать отчет каждую неделю, введите 7. Каждый день 1.', 
			]);
		
		}
        
	}

	public static function sendAutoTime($chat_id) {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$response = $telegram->sendMessage([
			'chat_id' => $chat_id,
			'parse_mode' => 'html',
			'text' => 'В какое время суток? Например, 21:00', 
		]);
	}

	public static function sendAutoFinish($chat_id) {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();
		$response = $telegram->sendMessage([
			'chat_id' => $chat_id,
			'parse_mode' => 'html',
			'text' => 'Спасибо. Вы будете получать отчет раз в '
			.$telegramAccount->auto_period.
			' '.\Lang::choice('день|дня|дней', $telegramAccount->auto_period , [], 'ru').' в '.$telegramAccount->autoTime() .'.', 
		]);
	}

	


	public static function sendStop($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();

		if ($telegramAccount) {

			$telegramAccount->is_auto = false;
			$telegramAccount->save();

			$response = $telegram->sendMessage([
				'chat_id' => $chat_id,
				'parse_mode' => 'html',
				'text' => 'Отчет остановлен.', 
			]);
		}
	}

	public static function sendNow($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();

		if ($telegramAccount) {
			$activeMonitorings = $telegramAccount->user->activeMonitorings;
			$is_empty = true;
			foreach ($activeMonitorings as $monitoring) {
				$is_empty = false;
				$text = "";
				$text .= "
				";
				$text .= "
				";
				$text .= '<b>'.$monitoring->string_header."
".$monitoring->getInfo().'</b>';
				$text .= "
				";
				if ( ($monitoring->active_second+60) < todaySecondNow() ) {

					// Общий контроль: %
					$text .= "Общий контроль: ".$monitoring->controlPercent()."%
					";
					// Google: %
					$text .=  "Google: ".$monitoring->controlPercent('g')."%
					";
					// Google Mobile: %
					$text .=  "Google Mobile: ".$monitoring->controlPercent('gm')."%
					";
					// Google Новости: %
					$text .=  "Google Новости: ".$monitoring->controlPercent('gn')."%
					";
					// Google Изображения: %
					$text .=  "Google Изображения: ".$monitoring->controlPercent('gi')."%
					";
					// Google Видео: %
					$text .=  "Google Видео: ".$monitoring->controlPercent('gv')."%
					";
					// Youtube: %
					$text .=  "Youtube: ".$monitoring->controlPercent('yo')."%
					";
				} else {

					$resent_second = $monitoring->active_second+600;

					$telegramResent = TelegramResent::where([
						'chat_id' => $telegramAccount->chat_id,
						'monitoring_id' => $monitoring->id,
						])
					->count();

					if (!$telegramResent) {
						TelegramResent::create([
							'chat_id' => $telegramAccount->chat_id,
							'monitoring_id' => $monitoring->id,
							'resent_second' => $resent_second ,
						]);
					}
					
					$text .= 'Отчет будет готов в ' . secondToTime($resent_second);
				}

				$response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
					'text' => $text, 
				]);

			}

			if ($is_empty) {
				$response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
					'text' => 'У вас нет активных мониторингов.', 
				]);
			}

		}
        
	}

	public static function resendNow($chat_id, $monitoring_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
		$monitoring = Monitoring::where(['id' => $monitoring_id])->first();


				$text = "";
				$text .= "
				";
				$text .= "
				";
				$text .= '<b>'.$monitoring->string_header."
".$monitoring->getInfo().'</b>';
				$text .= "
				";
				

		// Общий контроль: %
		$text .= "Общий контроль: ".$monitoring->controlPercent()."%
		";
		// Google: %
		$text .=  "Google: ".$monitoring->controlPercent('g')."%
		";
		// Google Mobile: %
		$text .=  "Google Mobile: ".$monitoring->controlPercent('gm')."%
		";
		// Google Новости: %
		$text .=  "Google Новости: ".$monitoring->controlPercent('gn')."%
		";
		// Google Изображения: %
		$text .=  "Google Изображения: ".$monitoring->controlPercent('gi')."%
		";
		// Google Видео: %
		$text .=  "Google Видео: ".$monitoring->controlPercent('gv')."%
		";
		// Youtube: %
		$text .=  "Youtube: ".$monitoring->controlPercent('yo')."%
		";
		$response = $telegram->sendMessage([
			'chat_id' => $chat_id,
			'parse_mode' => 'html',
			'text' => $text, 
		]);

		
        
	}

	public static function sendStart($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
        $response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
			'text' => "<b>Это бот контроля информационного поля.</b> 
Чтобы продолжить, введите ваш токен владельца.
Получить его можно по адресу 
monitoring.pr-consulting.com.ua > Авторизация >  Настройки > Аккаунт > Токен владельца", 
        ]);
	}
	public static function sendHelp($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
        $response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
		  'text' => "
			<b>Доступные команды:</b>
/now - получить отчет на сегодня, прямо сейчас.
/auto - получать отчет регулярно и автоматически, в заданное время.
/gtoken - Создает гостевой токен. Команда доступна только пользователю с токеном владельца.
/help - Выводит инструкцию.
/stop - Останавливает все автоотчеты текущего пользователя.
		  ", 
        ]);
	}
	public static function sendToken($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
        $response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
		  'text' => "<b>Выберите отчет:</b> 
/now – получить отчет на сегодня, прямо сейчас
/auto – получать отчет регулярно и автоматически, в заданное время
		  ", 
        ]);
	}
	
	public static function sendBusy($chat_id)
    {
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		
        $response = $telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'html',
		  'text' => "Токен владельца уже используется.", 
        ]);
	}


}
