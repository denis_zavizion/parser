@extends('app')

@section('content')

    <a href="{{ route('monitoring.index') }}" class="settings-fixed-icon"><i class="fa fa-gears"></i></a>

    @foreach($monitorings as $key => $monitoring)
        @include('monitoring/partials/_item',[ 'minitoring' => $monitoring, 'is_home' => true ])
    @endforeach

@endsection
