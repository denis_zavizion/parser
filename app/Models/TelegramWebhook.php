<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class TelegramWebhook {

	private $webhook_data;

	function __construct($data) {

		$this->webhook_data = $data;

		$chat_id = $this->webhook_data['message']['chat']['id'];
		
		if ($this->isCommand('/start')) {
			TelegramMessage::sendStart($chat_id);
			return;
		}

		if ($this->isCommand('/help')) {
			TelegramMessage::sendHelp($chat_id);
			return;
		}		

		if ($this->isKnownСhat()) {

			if ($this->isCommand('/now')) {
				TelegramMessage::sendNow($chat_id);
				return;
			}
			if ($this->isCommand('/gtoken')) {
				TelegramMessage::sendGtoken($chat_id);
				return;
			}
			if ($this->isCommand('/stop')) {
				TelegramMessage::sendStop($chat_id);
				return;
			}
			if ($this->isCommand('/auto')) {
				TelegramMessage::sendAuto($chat_id);
				return;
			}
			$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();
			if ($telegramAccount->previous_command === '/auto_auto_period') {
				$message = trim($this->webhook_data['message']['text']);
				if (is_numeric($message) && $message > 0) {

					$telegramAccount->auto_period = $message;
					$telegramAccount->previous_command = '/auto_auto_second';
					$telegramAccount->save();

					TelegramMessage::sendAutoTime($chat_id);

				} else {
					TelegramMessage::sendAuto($chat_id);
				}
			} else if ($telegramAccount->previous_command === '/auto_auto_second') {
				$message = trim($this->webhook_data['message']['text']);
				$seconds = $this->messageToSeconds($message);

				if ($seconds) {
					$telegramAccount->auto_second = $seconds;
					$telegramAccount->auto_trigger = 0;
					$telegramAccount->is_auto = 1;
					$telegramAccount->previous_command = 'none';
					$telegramAccount->save();

					TelegramMessage::sendAutoFinish($chat_id);
				} else {
					TelegramMessage::sendAutoTime($chat_id);
				}

			}				

		} else {
			$this->parseToken();
		}

	}

	private function messageToSeconds($message)
    {
		$arr = explode(":", $message);

        if (isset($arr[0]) && isset($arr[1]) && is_numeric($arr[0]) && is_numeric($arr[1]) && $arr[0] >= 0 && $arr[1] >= 0) {
			$sum = (int)$arr[0]*3600;
			$sum += (int)$arr[1]*60;

			if ($sum <= 86400) {
				return $sum;
			}
			
        }

		return false;
	}
	

	private function isKnownСhat()
    {
		$chat_id = $this->webhook_data['message']['chat']['id'];
		$telegramAccount = TelegramAccount::where(['chat_id' => $chat_id])->first();
		if ($telegramAccount) {
			return true;
		}
		return false;
	}


	private function isCommand($command)
    {
		$message = trim($this->webhook_data['message']['text']);

		if (strpos($command, $message) !== false) {
			return true;
		}
		return false;
	}
	
	
	///
	private function parseToken()
    {
		$chat_id = $this->webhook_data['message']['chat']['id'];
		$message = trim($this->webhook_data['message']['text']);

		if (isset($this->webhook_data['message']['from']['last_name'])) {
			$chat_name = $this->webhook_data['message']['from']['first_name']
			.' '.$this->webhook_data['message']['from']['last_name'];
		} else {
			$chat_name = $this->webhook_data['message']['from']['first_name'];
		}

        if (strlen($message) > 4 && strlen($message) < 10) {
			
			$telegramAccount = TelegramAccount::where(['token' => $message])->first();

			if ($telegramAccount) {
				
				if(!$telegramAccount->is_guest &&  $telegramAccount->chat_id > 0 ) {
					TelegramMessage::sendBusy($chat_id);
				} else {
					$telegramAccount->chat_id = $chat_id;
					$telegramAccount->chat_name = $chat_name;
					$telegramAccount->save();
					TelegramMessage::sendToken($chat_id);
				}
				
			}
			
		}

    }

}
