@extends('app')

@section('content')
    <div class="login-content">
        <div class="form-block">
            <div class="login-title">Мониторинг <br>контроля информационного поля</div>

            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-row @if ($errors->has('login')) has-error @endif">
                    <label class="form-label"><span>Логин</span></label>
                    <div class="inp-box">
                        {!! Form::text('login', null, ['class' => 'input-text']) !!}
                        @if ($errors->has('login')) <p class="help-block">{{ $errors->first('login') }}</p> @endif
                    </div>
                </div>

                <div class="form-row @if ($errors->has('password')) has-error @endif">
                    <label class="form-label"><span>Пароль</span></label>
                    <div class="inp-box">
                        {!! Form::password('password', null, ['class' => 'input-text']) !!}
                        @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    </div>
                </div>

                <div class="submit-row">
                    <div class="inp-box"><input type="submit" class="btn" value="Вход"></div>
                </div>

            </form>
        </div>
    </div><!-- end login-content -->


@endsection