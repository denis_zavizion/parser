<?php

namespace App\Http\Controllers;

use App\Models\Monitoring;
use App\Models\Country;
use App\Models\Language;
use Illuminate\Http\Request;

class MonitoringController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $monitorings = Monitoring::where(['user_id' => $user->id])
            ->latest()
            ->paginate(10)
        ;

        return view('monitoring.index', compact('monitorings'));
    }

    public function create(Request $request)
    {
        $user = auth()->user();

        $monitoring = new Monitoring();

        $fields = $request->all();

        if ($fields) {
            $fields['user_id'] = $user->id;
            $fields = $monitoring->beforeValidate($fields);
            if (in_array(weekdayNow(), $fields['date_scheduled'][1]) && $fields['active_second'] >= todaySecondNow()) {
                $fields['is_active_today'] = true;
            }

            $this->validate($request, Monitoring::rules());

            try{
                $monitoring = Monitoring::create($fields);
            }
            catch(\Exception $e){
                $message = $e->getMessage();
                \Log::error('MonitoringController create '.$message);
            }

            $monitoring->createIndividualTables();

            return redirect('monitorings');
        }

        $countries = Country::pluck('country', 'id');
        $presets = $this->getPresets();
        $languages = Language::pluck('name', 'id');
        

        return view('monitoring.new', compact(
            'monitoring',
            'languages',
            'presets',
            'countries'
        ));
    }

    public function edit(Request $request, $id)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {

            $fields = $request->all();

            if ($fields) {

                $fields = $monitoring->beforeValidate($fields);

                $this->validate($request, Monitoring::rules());

                try{
                    $monitoring->update($fields);
                }
                catch(\Exception $e){
                    $message = $e->getMessage();
                    \Log::error('MonitoringController edit '.$message);
                }
                return redirect('monitorings');
            }

            $countries = Country::pluck('country', 'id');

            $presets = $this->getPresets();

            $languages = Language::pluck('name', 'id');

            return view('monitoring.edit', compact(
                'monitoring',
                'languages',
                'presets', 
                'countries'
            ));
        }

        abort(404);

    }

    public function view(Request $request, $id)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {

            return view('monitoring.view', compact('monitoring'));
        }

        abort(404);

    }

    public function delete($id)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {

            try{
                $monitoring->delete();
            }
            catch(\Exception $e){
                $message = $e->getMessage();
                \Log::error('MonitoringController delete '.$message);
            }

            return redirect('monitorings');
        }

        abort(404);
    }

    public function ajaxReset(Request $request)
    {
        $monitoring_id = $request->monitoring_id;

        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $monitoring_id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $monitoring->reset();
            return \Response::json('Ok');
        }
    }

    public function ajaxResetGraph(Request $request)
    {
        $monitoring_id = $request->monitoring_id;
        $type = $request->type;

        $monitoring = Monitoring::where(['id' => $monitoring_id])->first();

        if ($monitoring) {
            $result = $monitoring->graph($type, true);
            return \Response::json($result);
        }
    }

    public function ajaxStatus(Request $request)
    {
        $monitoring_id = $request->monitoring_id;
        $val = $request->val;

        try{

            $monitoring = Monitoring::where(['id' => $monitoring_id])->first();
            $status = $val == 'active' ? Monitoring::STATUS_NOT_ACTIVE : Monitoring::STATUS_ACTIVE;
            $monitoring->current_status_string = $status;
            $monitoring->save();

        }
        catch(\Exception $e){
            $message = $e->getMessage();
            \Log::error('MonitoringController ajaxStatus'.$message);
        }

        return \Response::json('Ok');

    }

    private function getPresets()
    {
        $arr = [];
        $countries = Country::all();
        foreach($countries as $country) {
            $arr[$country->id] = $country->country_proxy;
        }
       
        return $arr;
    }

}
