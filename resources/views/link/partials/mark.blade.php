<div class="mark-area">
    <div class="result-check @if( $link->mark == 0 || $link->mark == 3 ) neutral @endif">
        <a href="#modal_result_{{ $key }}" data-toggle="modal" data-monitoring_id="{{ $monitoring->id }}" data-link_id="{{ $link->id }}" class="item-gear datemark-btn @if($link->mark == 3 ) hide @endif"><i class="fa fa-gears"></i></a>

        <input type="checkbox" class="switch-checkbox "
               data-monitoring_id="{{ $monitoring->id }}" data-link_id="{{ $link->id }}" data-type="{{ $type }}"
               @if( $link->mark == 1 ) checked @endif
               @if($link->mark == 3 ) disabled @endif
                >
    </div>
    <div class="mark-3">
        <input id="mark-3-{{ $link->id }}" type="checkbox" class="switch-neutral"
               data-monitoring_id="{{ $monitoring->id }}" data-link_id="{{ $link->id }}" data-type="{{ $type }}"
               @if( $link->mark == 3 ) checked @endif >
        <label for="mark-3-{{ $link->id }}">исключить линк</label>
    </div>
</div>

