<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToMonitoringPresetAndLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('table_all_strings', function (Blueprint $table) {
            $table->integer('preset_id')->unsigned()->default(1);
            $table->integer('language_id')->unsigned()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_all_strings', function (Blueprint $table) {
            $table->dropColumn('preset_id');
            $table->dropColumn('language_id');
        });
    }
}
