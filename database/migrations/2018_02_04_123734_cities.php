<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('city');

            // Indexes
            $table->index('country_id');

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
            ;
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign('cities_country_id_foreign');
        });
        Schema::dropIfExists('cities');
    }
}
