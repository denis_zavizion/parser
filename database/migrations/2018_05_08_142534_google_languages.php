<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GoogleLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('google_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('english_name');
            $table->string('value');
        });
        
        DB::table('google_languages')->insert([
            'name' => 'Любой',
            'english_name' => 'Any',
            'value' => '',
        ]);

        DB::table('google_languages')
        ->insert(['name' => 'Арабский', 'english_name' => 'Arabic','value' => 'lang_ar',]);
        DB::table('google_languages')
        ->insert(['name' => 'Китайский упрощенный', 'english_name' => 'Chinese (Simplified)','value' => 'lang_zh-CN',]);
        DB::table('google_languages')
        ->insert(['name' => 'Китайский традиционный', 'english_name' => 'Chinese (Traditional)','value' => 'lang_zh-TW',]);
        DB::table('google_languages')
        ->insert(['name' => 'Чешский', 'english_name' => 'Czech','value' => 'lang_cs',]);
        DB::table('google_languages')
        ->insert(['name' => 'Датский', 'english_name' => 'Danish','value' => 'lang_da',]);

        DB::table('google_languages')
        ->insert(['name' => 'Голландский', 'english_name' => 'Dutch','value' => 'lang_nl',]);
        DB::table('google_languages')
        ->insert(['name' => 'Английский', 'english_name' => 'English','value' => 'lang_en',]);
        DB::table('google_languages')
        ->insert(['name' => 'Эстонский', 'english_name' => 'Estonian','value' => 'lang_et',]);
        DB::table('google_languages')
        ->insert(['name' => 'Финский', 'english_name' => 'Finnish','value' => 'lang_fi',]);
        DB::table('google_languages')
        ->insert(['name' => 'Французский', 'english_name' => 'French','value' => 'lang_fr',]);

        DB::table('google_languages')
        ->insert(['name' => 'Немецкий', 'english_name' => 'German','value' => 'lang_de',]);
        DB::table('google_languages')
        ->insert(['name' => 'Греческий', 'english_name' => 'Greek','value' => 'lang_el',]);
        DB::table('google_languages')
        ->insert(['name' => 'Иврит', 'english_name' => 'Hebrew','value' => 'lang_iw',]);
        DB::table('google_languages')
        ->insert(['name' => 'Венгерский', 'english_name' => 'Hungarian','value' => 'lang_hu',]);
        DB::table('google_languages')
        ->insert(['name' => 'Исландский', 'english_name' => 'Icelandic','value' => 'lang_is',]);

        DB::table('google_languages')
        ->insert(['name' => 'Итальянский', 'english_name' => 'Italian','value' => 'lang_it',]);
        DB::table('google_languages')
        ->insert(['name' => 'Японский', 'english_name' => 'Japanese','value' => 'lang_ja',]);
        DB::table('google_languages')
        ->insert(['name' => 'Корейский', 'english_name' => 'Korean','value' => 'lang_ko',]);
        DB::table('google_languages')
        ->insert(['name' => 'Латышский', 'english_name' => 'Latvian','value' => 'lang_lv',]);
        DB::table('google_languages')
        ->insert(['name' => 'Литовский', 'english_name' => 'Lithuanian','value' => 'lang_lt',]);

        DB::table('google_languages')
        ->insert(['name' => 'Норвежский', 'english_name' => 'Norwegian','value' => 'lang_no',]);
        DB::table('google_languages')
        ->insert(['name' => 'Португальский', 'english_name' => 'Portuguese','value' => 'lang_pt',]);
        DB::table('google_languages')
        ->insert(['name' => 'Польский', 'english_name' => 'Polish','value' => 'lang_pl',]);
        DB::table('google_languages')
        ->insert(['name' => 'Румынский', 'english_name' => 'Romanian','value' => 'lang_ro',]);
        DB::table('google_languages')
        ->insert(['name' => 'Русский', 'english_name' => 'Russian','value' => 'lang_ru',]);

        DB::table('google_languages')
        ->insert(['name' => 'Испанский', 'english_name' => 'Spanish','value' => 'lang_es',]);
        DB::table('google_languages')
        ->insert(['name' => 'Шведский', 'english_name' => 'Swedish','value' => 'lang_sv',]);
        DB::table('google_languages')
        ->insert(['name' => 'Турецкий', 'english_name' => 'Turkish','value' => 'lang_tr',]);
        DB::table('google_languages')
        ->insert(['name' => 'Украинский', 'english_name' => 'Ukrainian','value' => 'lang_uk',]);
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_languages');
    }
}
