<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class TelegramAccount extends Model {

	protected $table = 'telegram_account';

	protected $fillable = [
		'user_id',
		'token',
		'chat_id',
		'chat_name',
		'is_guest',
		'is_auto',
		'auto_second',
		'auto_period',
		'auto_trigger',
		'previous_command',
		
	];

	/*
    |--------------------------------------------------------------------------
    | Validation Rules
    |--------------------------------------------------------------------------
    */

	public static function rules()
	{
		$rules = [
			'user_id' => 'required|integer|exists:users,id',
			'token' => 'required|string',
			'chat_id' => 'sometimes|integer',
			'chat_name' => 'sometimes|string',
			'is_guest' => 'required|boolean',
			'is_auto' => 'required|boolean',
			'auto_second' => 'required|integer|min:0|max:86400',
			'auto_period' => 'required|integer|min:0|max:100',
			'auto_trigger' => 'required|integer|min:0|max:100',
			'previous_command' => 'sometimes|string',
		];

		return $rules;
	}

	/*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/*
    |--------------------------------------------------------------------------
    | Static Methods
    |--------------------------------------------------------------------------
	*/
	public static function resetActiveToday()
	{
		/// auto_trigger когда = 0 то активно сегодня
		$telegramAccounts = TelegramAccount::where(['is_auto' => true])->get();

		foreach ($telegramAccounts as $telegramAccount) {
			if ($telegramAccount->auto_trigger > 0 ) {
				$telegramAccount->auto_trigger = $telegramAccount->auto_trigger-1;
				$telegramAccount->save();
			}
		}
	}

	public static function sentActiveMessages()
	{
		/// auto_trigger когда = 0 то активно сегодня
		
		$telegramAccounts = TelegramAccount::where('auto_second','<', todaySecondNow())
		->where('is_auto', 1)
		->where('auto_trigger', 0)
		->get();

		foreach ($telegramAccounts as $telegramAccount) {
			
			$telegramAccount->auto_trigger = $telegramAccount->auto_period;
			$telegramAccount->save();
			TelegramMessage::sendNow($telegramAccount->chat_id);
			
		}
	}
	public static function resentActiveMessages()
	{

		$telegramResents = TelegramResent::where('resent_second','<', todaySecondNow())
		->get();

		foreach ($telegramResents as $telegramResent) {
			TelegramMessage::resendNow($telegramResent->chat_id, $telegramResent->monitoring_id);
			$telegramResent->delete();
		}
	}

	/*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
	*/
	public function autoTime()
	{
		return secondToTime($this->auto_second);
	}
	
	
}
