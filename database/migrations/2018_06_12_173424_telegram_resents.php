<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelegramResents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_resent', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('chat_id')->unsigned();
            $table->integer('monitoring_id')->unsigned();
            $table->integer('resent_second')->unsigned();
            /// indexes
            $table->index('resent_second');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_resent');
    }
}
