<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Monitoring;
use App\Models\Queue;
use App\Models\TelegramAccount;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Monitoring::resetActiveToday();
        })->dailyAt('00:01');

        $schedule->call(function () {
            TelegramAccount::resetActiveToday();
        })->dailyAt('00:01');

        $schedule->call(function () {
            Monitoring::makeActiveQueues();
        })->everyMinute();

        $schedule->call(function () {
            TelegramAccount::sentActiveMessages();
        })->everyMinute();
        $schedule->call(function () {
            TelegramAccount::resentActiveMessages();
        })->everyMinute();

        $schedule->call(function () {
            Queue::parseFirstPack();
        })->everyMinute();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
