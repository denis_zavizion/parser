<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;
use App\Models\TelegramAccount;

class UserController extends Controller
{
    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('user.login');
    }

    public function username()
    {
        return 'login';
    }

    public function index()
    {
        $users = User::orderBy('login', 'ASC')
            ->paginate(10)
        ;

        return view('user.index', compact('users'));
    }

    public function cleartoken(Request $request, $id)
    {

        $user = User::where(['id' => auth()->user()->id])->first();

        $telegramAccount = TelegramAccount::where(['id' => $id])->first();

        if ($telegramAccount) {
            if ($telegramAccount->is_guest) {
                $telegramAccount->delete();
            } else {
                $telegramAccount->chat_id = null;
                $telegramAccount->chat_name = null;
                $telegramAccount->token = generateToken($user->id*100);
                $telegramAccount->save();
            }
        }

        return redirect('/user/edit/'. $user->id);
    }

    public function create(Request $request)
    {


        $user = new User();

        $fields = $request->all();

        if ($fields) {
            $this->validate($request, User::createRules());
            try{
                $fields['password'] = bcrypt($fields['new_password']);
                $new = User::create($fields);
                $new->createTelegramAccount();
            }
            catch(\Exception $e){
                $message = $e->getMessage();
                \Log::error('UserController create '.$message);
            }

            return redirect('users');
        }

        return view('user.new', compact('user'));
    }

    public function edit(Request $request, $id)
    {
        if (auth()->user()->isAdmin()) {
            $user = User::where(['id' => $id])->first();
        } else {
            $user = User::where(['id' => auth()->user()->id])->first();
        }

        if ($user) {

            $user->createTelegramAccount();
            $fields = $request->all();

            if ($fields) {

                $this->validate($request, User::editRules($user));

                try{
                    if(isset($fields['new_password'])) {
                        $fields['password'] = bcrypt($fields['new_password']);
                    }
                    $user->update($fields);
                }
                catch(\Exception $e){
                    $message = $e->getMessage();
                    \Log::error('UserController edit '.$message);
                }
                if (auth()->user()->isAdmin()) {
                    return redirect('users');
                }
            }

            return view('user.edit', compact('user'));
        }

        abort(404);

    }

    public function delete($id)
    {

        $user = User::where(['id' => $id])->first();

        if ($user) {

            try{
                $user->delete();
            }
            catch(\Exception $e){
                $message = $e->getMessage();
                \Log::error('UserController delete '.$message);
            }

            return redirect('users');
        }

        abort(404);
    }

    public function ajaxStatus(Request $request)
    {
        $user_id = $request->user_id;
        $val = $request->val;

        try{

            $user = User::where(['id' => $user_id])->first();
            $status = $val == 'active' ? 0 : 1;
            $user->status = $status;
            $user->save();

        }
        catch(\Exception $e){
            $message = $e->getMessage();
            \Log::error('UserController ajaxStatus'.$message);
        }

        return \Response::json('Ok');

    }

    public function ajaxRole(Request $request)
    {
        $user_id = $request->user_id;
        $val = $request->val;

        try{
            $user = User::where(['id' => $user_id])->first();
            $role = $val == 'admin' ? 0 : 1;
            $user->role = $role;
            $user->save();
        }
        catch(\Exception $e){
            $message = $e->getMessage();
            \Log::error('UserController ajaxRole'.$message);
        }

        return \Response::json('Ok');

    }
}
