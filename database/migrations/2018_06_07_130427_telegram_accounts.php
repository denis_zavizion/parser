<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelegramAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_account', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('token')->unique();
            $table->integer('chat_id')->unsigned()->nullable();
            $table->string('chat_name')->nullable();
            $table->boolean('is_guest')->default(true);   

            /// /auto
            $table->boolean('is_auto')->default(false);
            $table->integer('auto_second')->unsigned();/// время в секундах во сколько слать сообщение при auto_trigger = 0 сегодня
            $table->integer('auto_period')->unsigned();/// когда auto_trigger послал сообщение то auto_trigger = auto_period 
            $table->integer('auto_trigger')->unsigned();/// каждый день в 00:00  --  и когда он 0 значит этот день активный
            
            $table->string('previous_command');

            $table->timestamps();

            /// indexes
            $table->index('token');
            $table->index('user_id');

            /// foreign keys
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
     {
         Schema::table('telegram_account', function (Blueprint $table) {
            $table->dropForeign('telegram_account_user_id_foreign');
        });
        Schema::dropIfExists('telegram_account');
    }
}
