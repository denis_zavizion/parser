<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class Monitoring extends Model {

	const  STATUS_REMOVED = 0;
	const  STATUS_ACTIVE = 1;
	const  STATUS_NOT_ACTIVE = 2;

	const  MARK_NEW = 0;
	const  MARK_CONTROL = 1;
	const  MARK_NOT_CONTROL = 2;
	const  MARK_NEUTRAL = 3;

	protected $table = 'table_all_strings';

	protected $fillable = [
		'user_id',
		'active_second',
		'is_active_today',
		'string',
		'string_table',
		'string_header',
		'parser_status',
		'country_id',
		'city_id',
		'domain_id',

		'language_id',
		'preset_id',

		'links_per_page',
		'pages_count',
		'language',
		'interface_language',
		'current_status',
		'current_status_string',
		'date_scheduled',
		'last_parsing_date',
		'next_parsing_date',
	];

	protected $casts = [
		'date_scheduled' => 'array',
		'parser_status' => 'array',
	];

	protected $attributes = [
		'pages_count' => 100,
		'date_scheduled' => '["00:00", ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]]',
		'parser_status' => '["google", "google_mobile", "google_news", "google_image", "google_video", "youtube"]',
	];

	/*
    |--------------------------------------------------------------------------
    | Validation Rules
    |--------------------------------------------------------------------------
    */

	public static function rules()
	{
		$rules = [
			'string' => 'required|string',
			'pages_count' => 'required|string',
		];

		return $rules;
	}

	public function beforeValidate($fields) {
		$fields['string_table'] = str_slug($fields['string']);
		$fields['string_header'] = str_limit($fields['string'], $limit = 100, $end = '...');
		$fields['links_per_page'] = 10;
		$fields['active_second'] = timeToSecond($fields['date_scheduled'][0])+70;
		$fields['is_active_today'] = false;

		if (in_array(weekdayNow(), $fields['date_scheduled'][1]) && $fields['active_second'] >= todaySecondNow()) {
			$fields['is_active_today'] = true;
		}

		return $fields;
	}

	/*
    |--------------------------------------------------------------------------
    | Static Methods
    |--------------------------------------------------------------------------
    */

	/**
	 * когда у мониторинга (active_second < todaySecondNow()  и  is_active_today = true)
	 * то создать запросы для него и is_active_today = false
	 */
	public static function makeActiveQueues()
	{
		$monitorings = Monitoring::where('is_active_today','=', 1)
			->where('active_second','<', todaySecondNow())
			->get()
		;
		foreach ($monitorings as $monitoring) {
			$monitoring->makeQueues();
			$monitoring->notActiveNow();
		}

	}

	/**
	 * новый день
	 * сброс мониторингов на новый день  is_active_today = true если нужно пропарсить сегодня
	 */
	public static function resetActiveToday()
	{
		$monitorings = Monitoring::where('date_scheduled', 'like', '%' . weekdayNow() . '%')
			->where('active_second','>', todaySecondNow())
			->update(['is_active_today' => 1])
		;
	}

	/*
	|--------------------------------------------------------------------------
	| Relationship Methods
	|--------------------------------------------------------------------------
	*/

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function city()
	{
		return $this->belongsTo('App\Models\City');
	}

	public function country()
	{
		return $this->belongsTo('App\Models\Country');
	}
	public function preset()
	{
		return $this->belongsTo('App\Models\Country');
	}

	public function lang()
	{
		return $this->belongsTo('App\Models\Language','language_id');
	}

	public function queues()
	{
		return $this->hasMany(\App\Models\Queue::class, 'string_id', 'id');
	}

	public function qos()
	{
		return $this->hasMany(\App\Models\Go::class, 'string_id', 'id');
	}

	/*
	|--------------------------------------------------------------------------
	| Methods
	|--------------------------------------------------------------------------
	*/

	public function isOn($str){
		/// '["google", "google_mobile", "google_news", "google_image", "google_video", "youtube"]';
		return in_array($str, $this->parser_status);
	}

	public function makeQueues()
	{
		foreach($this->parser_status as $queue) {

			try{
				Queue::create([
					'user_id' => $this->user_id,
					'string_id'  => $this->id,
					'json_string' => [
						'type' => $queue,
						'query' => $this->string,
						'links_count' =>  $this->pages_count,
						'domain' => $this->country->domain->domain,
                        'country' => $this->country->country,
                        'country_en' => $this->country->en,
						'language' => $this->lang->value,
                        'proxy_preset' => $this->preset->proxy_preset,
					],
				]);
			}
			catch(\Exception $e){
				$message = $e->getMessage();
				\Log::error('Monitoring makeQueues()'.$message);
			}

		}
	}

	public function makeExtraQueues()
	{
		foreach($this->parser_status as $queue) {

			try{
				DB::table(
					'string_queue'
				)->insert([
					'user_id' => $this->user_id,
					'string_id'  => $this->id,
					'json_string' => json_encode([
						'type' => $queue,
						'query' => $this->string,
						'links_count' =>  $this->pages_count,
						'domain' => $this->country->domain->domain,
						'country' => $this->country->country,
						'country_en' => $this->country->en,
						'language' => $this->lang->value,
                        'proxy_preset' => $this->preset->proxy_preset,
					]),
					'created_at' => '1999-12-12 00:00:00',
					'updated_at' => '1999-12-12 00:00:00',
				]);
			}
			catch(\Exception $e){
				$message = $e->getMessage();
				\Log::error('Monitoring makeExtraQueues()'.$message);
			}

		}
	}

	public function notActiveNow()
	{
		$this->update([
			'is_active_today' => 0,
		]);
	}

	public function  createIndividualTables() {
		Schema::create('string_'.$this->id, function (Blueprint $table) {
			$table->increments('id');
			$table->timestamp('parsing_date')->nullable();
			$table->string('link', 757)->nullable();
			$table->tinyInteger('mark')->unsigned()->default(0);
			$table->string('domain')->nullable();
			$table->string('parsed_by');
			$table->string('anchor')->nullable();
			$table->text('snippet')->nullable();
			$table->timestamp('serp_time')->nullable();
			$table->string('username')->nullable();
			$table->integer('views')->nullable();
			$table->string('timing')->nullable();
			$table->integer('position')->nullable();
			$table->text('preview')->nullable();

			// Indexes
			$table->index('link');
		});

		Schema::create('string_stat_'.$this->id, function (Blueprint $table) {
			$table->increments('id');
			$table->timestamp('date')->nullable();

			$table->float('aggregate_control');
			$table->float('aggregate_uncontrol');
			$table->float('aggregate_neutral');

			$table->integer('g_stats_control');
			$table->integer('g_stats_uncontrol');
			$table->integer('g_stats_neutral');

			$table->integer('gm_stats_control');
			$table->integer('gm_stats_uncontrol');
			$table->integer('gm_stats_neutral');

			$table->integer('gn_stats_control');
			$table->integer('gn_stats_uncontrol');
			$table->integer('gn_stats_neutral');

			$table->integer('gi_stats_control');
			$table->integer('gi_stats_uncontrol');
			$table->integer('gi_stats_neutral');

			$table->integer('gv_stats_control');
			$table->integer('gv_stats_uncontrol');
			$table->integer('gv_stats_neutral');

			$table->integer('yo_stats_control');
			$table->integer('yo_stats_uncontrol');
			$table->integer('yo_stats_neutral');

		});
	}

	private function  deleteIndividualTables() {
		Schema::dropIfExists('string_'.$this->id);
		Schema::dropIfExists('string_stat_'.$this->id);
	}

	public function getLnksForDay($day, $parsed_by)
	{
		return DB::table('string_'.$this->id)
			->where('parsed_by', $parsed_by)
			->whereDate('parsing_date', '=', $day)
			;
	}

	public function getLink($link_id)
	{
		 $link = DB::table('string_'.$this->id)
				->where('id', $link_id)
			 	->first()
			;

		return $link;
	}

	public function reset()
	{
		$this->makeExtraQueues();

		Queue::parseFirstPack();

		$this->notActiveNow();
	}

	public function buildStatistics($day)
	{
		$day = date("Y-m-d", strtotime($day));

		$data =  DB::table('string_'.$this->id)
			->select('mark','parsed_by')
			->where('mark', '<>', 0)
			->whereDate('parsing_date', '=', $day)
			->get()
			;


		DB::table('string_stat_'.$this->id)
			->whereDate('date', '=', $day)
			->delete()
		;
		/**
	 	Сумма значений полей c префиксом _control в таблице Очередь заданий
		string_queue, где дата=сегодня, деленная на количество этих же полей, имеющих значение (не null)
		*/

		$all_sum = count($data);

		$g_stats_control = 0;
		$g_stats_uncontrol = 0;
		$g_stats_neutral = 0;

		$gm_stats_control = 0;
		$gm_stats_uncontrol = 0;
		$gm_stats_neutral = 0;

		$gn_stats_control = 0;
		$gn_stats_uncontrol = 0;
		$gn_stats_neutral = 0;

		$gi_stats_control = 0;
		$gi_stats_uncontrol = 0;
		$gi_stats_neutral = 0;

		$gv_stats_control = 0;
		$gv_stats_uncontrol = 0;
		$gv_stats_neutral = 0;

		$yo_stats_control = 0;
		$yo_stats_uncontrol = 0;
		$yo_stats_neutral = 0;

		foreach ($data as $item) {

			if ($item->parsed_by == 'g') {
				$g_stats_control += $item->mark == 1 ? 1 : 0;
				$g_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$g_stats_neutral += $item->mark == 3 ? 1 : 0;
			}

			if ($item->parsed_by == 'gm') {
				$gm_stats_control += $item->mark == 1 ? 1 : 0;
				$gm_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$gm_stats_neutral += $item->mark == 3 ? 1 : 0;
			}

			if ($item->parsed_by == 'gn') {
				$gn_stats_control += $item->mark == 1 ? 1 : 0;
				$gn_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$gn_stats_neutral += $item->mark == 3 ? 1 : 0;
			}
			
			if ($item->parsed_by == 'gi') {
				$gi_stats_control += $item->mark == 1 ? 1 : 0;
				$gi_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$gi_stats_neutral += $item->mark == 3 ? 1 : 0;
			}
			
			if ($item->parsed_by == 'gv') {
				$gv_stats_control += $item->mark == 1 ? 1 : 0;
				$gv_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$gv_stats_neutral += $item->mark == 3 ? 1 : 0;
			}

			if ($item->parsed_by == 'yo') {
				$yo_stats_control += $item->mark == 1 ? 1 : 0;
				$yo_stats_uncontrol += $item->mark == 2 ? 1 : 0;
				$yo_stats_neutral += $item->mark == 3 ? 1 : 0;
			}

		}

		$aggregate_control = 0;
		$aggregate_uncontrol = 0;
		$aggregate_neutral = 0;

		if ($all_sum != 0) {

			$sum_control = $g_stats_control+$gm_stats_control+$gn_stats_control+$gi_stats_control+$gv_stats_control+$yo_stats_control;
			$aggregate_control = $sum_control / $all_sum;

			$sum_uncontrol = $g_stats_uncontrol+$gm_stats_uncontrol+$gn_stats_uncontrol+$gi_stats_uncontrol+$gv_stats_uncontrol+$yo_stats_uncontrol;
			$aggregate_uncontrol = $sum_uncontrol / $all_sum;


			$sum__neutral = $g_stats_neutral+$gm_stats_neutral+$gn_stats_neutral+$gi_stats_neutral+$gv_stats_neutral+$yo_stats_neutral;
			$aggregate_neutral = $sum__neutral / $all_sum;

		}

		DB::table('string_stat_'.$this->id)->insert([
			'date' => $day,

			'aggregate_control'  => $aggregate_control,
			'aggregate_uncontrol'  => $aggregate_uncontrol,
			'aggregate_neutral'  => $aggregate_neutral,

			'g_stats_control'  => $g_stats_control,
			'g_stats_uncontrol'  => $g_stats_uncontrol,
			'g_stats_neutral'  => $g_stats_neutral,

			'gm_stats_control'  => $gm_stats_control,
			'gm_stats_uncontrol'  => $gm_stats_uncontrol,
			'gm_stats_neutral'  => $gm_stats_neutral,

			'gn_stats_control'  => $gn_stats_control,
			'gn_stats_uncontrol'  => $gn_stats_uncontrol,
			'gn_stats_neutral'  => $gn_stats_neutral,

			'gi_stats_control'  => $gi_stats_control,
			'gi_stats_uncontrol'  => $gi_stats_uncontrol,
			'gi_stats_neutral'  => $gi_stats_neutral,

			'gv_stats_control'  => $gv_stats_control,
			'gv_stats_uncontrol'  => $gv_stats_uncontrol,
			'gv_stats_neutral'  => $gv_stats_neutral,

			'yo_stats_control'  => $yo_stats_control,
			'yo_stats_uncontrol'  => $yo_stats_uncontrol,
			'yo_stats_neutral'  => $yo_stats_neutral,
		]);
	}

	public function controlPercent($parsed_by = 'all', $day = null)
	{
		$result = 0;

        try{
            if (!$day) {
                $day = dayNow();
            }

            $stat =  DB::table('string_stat_'.$this->id)
                ->whereDate('date', '=', $day)
                ->first()
            ;

            if (!$stat) {
                $this->buildStatistics($day);
                $stat =  DB::table('string_stat_'.$this->id)
                    ->whereDate('date', '=', $day)
                    ->first()
                ;
            }


            if ($parsed_by == 'g') {

                $sum = $stat->g_stats_control + $stat->g_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->g_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'gm') {

                $sum = $stat->gm_stats_control + $stat->gm_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->gm_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'gn') {

                $sum = $stat->gn_stats_control + $stat->gn_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->gn_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'gi') {

                $sum = $stat->gi_stats_control + $stat->gi_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->gi_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'gv') {

                $sum = $stat->gv_stats_control + $stat->gv_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->gv_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'yo') {

                $sum = $stat->yo_stats_control + $stat->yo_stats_uncontrol ;
                if ($sum) {
                    $result = $stat->yo_stats_control / $sum *100;
                }

            }

            if ($parsed_by == 'all') {


                $result1 = 0;
                $result2 = 0;
                $result3 = 0;
                $result4 = 0;
                $result5 = 0;
                $result6 = 0;

                $i = 0;

                $sum = $stat->g_stats_control + $stat->g_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result1 = $stat->g_stats_control / $sum *100;
                }

                $sum = $stat->gm_stats_control + $stat->gm_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result2 = $stat->gm_stats_control / $sum *100;
                }

                $sum = $stat->gn_stats_control + $stat->gn_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result3 = $stat->gn_stats_control / $sum *100;
                }

                $sum = $stat->gi_stats_control + $stat->gi_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result4 = $stat->gi_stats_control / $sum *100;
                }

                $sum = $stat->gv_stats_control + $stat->gv_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result5 = $stat->gv_stats_control / $sum *100;
                }

                $sum = $stat->yo_stats_control + $stat->yo_stats_uncontrol ;
                if ($sum) {
                    $i++;
                    $result6 = $stat->yo_stats_control / $sum *100;
                }

                $percent_sum = roundH($result1)+roundH($result2)+roundH($result3)+roundH($result4)+roundH($result5)+roundH($result6);

                $result = 0;

                if ($i>0) {
                    $result = $percent_sum  / $i;
                }

            }

        }
        catch(\Exception $e){
            $message = $e->getMessage();
            \Log::error('Monitoring controlPercent-'.$message);
        }

		return roundH($result);

	}

	public  function  graph($parsed_by = 'all', $is_for_ajax = false) {

		$items =  DB::table('string_stat_'.$this->id)
			->limit(14)
			->orderBy('id', 'desc')
			->get()
		;


		$str = '[';
		$arr = [];

		$empty_days_count = 14 - count($items);

		while ( $empty_days_count>0 ) {
			$empty_days_count--;
			$str.=' 0, ';
			$arr[] = 0;
		}

		foreach ($items->reverse() as $item) {

			if ($parsed_by == 'g') {
				$sum = $item->g_stats_control + $item->g_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->g_stats_control / $sum * 100) ;
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}

			if ($parsed_by == 'gi') {
				$sum = $item->gi_stats_control + $item->gi_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->gi_stats_control / $sum * 100) ;
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}

			if ($parsed_by == 'gn') {
				$sum = $item->gn_stats_control + $item->gn_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->gn_stats_control / $sum * 100);
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}

			if ($parsed_by == 'gm') {
				$sum = $item->gm_stats_control + $item->gm_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->gm_stats_control / $sum * 100);
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}

			if ($parsed_by == 'gv') {
				$sum = $item->gv_stats_control + $item->gv_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->gv_stats_control / $sum * 100);
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}

			if ($parsed_by == 'yo') {
				$sum = $item->yo_stats_control + $item->yo_stats_uncontrol ;
				if ($sum) {
					$val = roundH($item->yo_stats_control / $sum * 100);
				} else $val = 0;

				$str.= $val. ', ';
				$arr[] = $val;
			}


			if ($parsed_by == 'all') {


				$var1 = 0;
				$var2 = 0;
				$var3 = 0;
				$var4 = 0;
				$var5 = 0;
				$var6 = 0;

				$i = 0;


					$sum = $item->g_stats_control + $item->g_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val1 = roundH($item->g_stats_control / $sum * 100) ;
					} else $val1 = 0;



					$sum = $item->gi_stats_control + $item->gi_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val2 = roundH($item->gi_stats_control / $sum * 100) ;
					} else $val2 = 0;



					$sum = $item->gn_stats_control + $item->gn_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val3 = roundH($item->gn_stats_control / $sum * 100);
					} else $val3 = 0;


					$sum = $item->gm_stats_control + $item->gm_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val4 = roundH($item->gm_stats_control / $sum * 100);
					} else $val4 = 0;



					$sum = $item->gv_stats_control + $item->gv_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val5 = roundH($item->gv_stats_control / $sum * 100);
					} else $val5 = 0;



					$sum = $item->yo_stats_control + $item->yo_stats_uncontrol ;
					if ($sum) {
						$i++;
						$val6 = roundH($item->yo_stats_control / $sum * 100);
					} else $val6 = 0;

				$percent_sum = roundH($val1)+roundH($val2)+roundH($val3)+roundH($val4)+roundH($val5)+roundH($val6);

				$val = 0;

				if ($i>0) {
					$val = $percent_sum  / $i;
				}

				$str.=  $val.', ';
				$arr[] = $val;


			}
		}

		$str .= ']';

		if ($is_for_ajax) {
			return $arr;
		}

		return $str;

	}

	public function delete()
	{

		try{
			$this->qos()->delete();

            $this->queues()->delete();

			$this->deleteIndividualTables();
			return parent::delete();
		}
		catch(\Exception $e){
			$message = $e->getMessage();
			\Log::error('Monitoring delete()'.$message);
		}
	}

	public function getActiveDates($link_id)
	{
		$first = DB::table('string_'.$this->id)
			->where('id', $link_id)
			->first()
		;

		$dates = DB::table('string_'.$this->id)
			->where('link', $first->link)
			->where('parsed_by', $first->parsed_by)
			->where('mark', self::MARK_CONTROL)
			->get()
		;

		$arr = [];
		foreach($dates as $item) {
			$arr[] = str_replace(' 00:00:00', '', $item->parsing_date);
		}

		return $arr;

	}

	public function pushDates($date, $link_id)
	{
		$user = auth()->user();

		$first = DB::table('string_'.$this->id)
			->where('id', $link_id)
			->first()
		;

		$mark = self::MARK_CONTROL; /// по умолчанию ставим активный
		/// найти и узнать mark
		$pusher = DB::table('string_'.$this->id)
			->where('link', $first->link)
			->whereDate('parsing_date', '>=', $date)
			->first()
		;

		if ($pusher && $pusher->mark != self::MARK_NOT_CONTROL) {
			$mark = self::MARK_NOT_CONTROL;
		}
		
		try{

			foreach ($user->monitorings as $monitoring) {
				DB::table('string_'.$monitoring->id)
					->where('link', $first->link)
					->where('parsed_by', $first->parsed_by)
					->whereDate('parsing_date', '>=', $date)
					->update(['mark' => $mark]);
				;
			}

			$dates = DB::table('string_'.$this->id)
				->where('link', $first->link)
				->where('parsed_by', $first->parsed_by)
				->whereDate('parsing_date', '>=', $date)
				->get();
			;

			foreach ($dates as $date) {
				$user->buildStatistics($date->parsing_date);
			}

		}
		catch(\Exception $e){
			$message = $e->getMessage();
			\Log::error('Monitoring pushDates()'.$message);
		}

		return $pusher->id.'-aaa-'.$pusher->mark;
	}

	public function isActive() {
		return $this->current_status_string == self::STATUS_ACTIVE;
	}

	public function graphCategories() {

		$i = 13;
		$str = '[';
		while ( $i > 0 ) {
			$str .= date("d", strtotime("-".$i." day")).', ';
			$i--;
		}
		return $str .= date("d").']';
	}

	public function getInfo() {
		return $this->country->domain->domain.' '.$this->country->country.'('.$this->lang->name.'-'. $this->preset->proxy_preset.') глубина'. $this->pages_count;
	}

}
