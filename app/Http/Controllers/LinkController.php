<?php

namespace App\Http\Controllers;

use App\Models\Monitoring;
use App\Models\Country;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /// all mobile news image video youtube
    
    public function all(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'g');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }

            return view('link.google', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function mobile(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'gm');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }

            return view('link.mobile', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function news(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'gn');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }

            return view('link.news', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function image(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'gi');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }

            return view('link.image', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function video(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'gv');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }

            return view('link.video', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function youtube(Request $request, $id ,$day, $mark = false)
    {
        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $query = $monitoring->getLnksForDay($day, 'yo');

            if ($mark == 'new') {
                $query->where( 'mark', Monitoring::MARK_NEW );
                $all_count = $query->count();
                $links = $query->limit(10)->get();
            } else {
                $all_count = $query->count();
                $links = $query->limit(10)->paginate(10);
            }
            return view('link.youtube', compact('monitoring', 'links', 'mark', 'all_count'));
        }

        abort(404);
    }

    public function ajaxMark(Request $request)
    {
        $link_id = $request->link_id;
        $monitoring_id = $request->monitoring_id;
        $check = $request->check;

        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $monitoring_id, 'user_id' => $user->id])->first();

        if ($monitoring) {


            $link = $monitoring->getLink($link_id);

            if ($check == 'on') {
                $user->linkMark($link->link, $link->parsed_by, dayNow(), Monitoring::MARK_CONTROL);
            } else if($check == 'neutral') {
                $user->linkMark($link->link, $link->parsed_by, dayNow(), Monitoring::MARK_NEUTRAL);
            } else {
                $user->linkMark($link->link, $link->parsed_by, dayNow(), Monitoring::MARK_NOT_CONTROL);
            }

            return \Response::json('Ok');
        }
    }

    public function ajaxDateMark(Request $request)
    {
        $link_id = $request->link_id;
        $monitoring_id = $request->monitoring_id;
        $status = $request->status;

        $user = auth()->user();

        $monitoring = Monitoring::where(['id' => $monitoring_id, 'user_id' => $user->id])->first();

        if ($monitoring) {
            $str = 'a';
            if (isset($status)) {
                $str = $monitoring->pushDates($status, $link_id);
            }

            $active_dates = $monitoring->getActiveDates($link_id);

            return \Response::json([
                'active_dates' => $active_dates,
                'msg' => $str,
            ]);

        }
    }

}
