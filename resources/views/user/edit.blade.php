@extends('app')

@section('content')
    @include('menu', ['page' => 'user.settings'])
    <div id="container">
        <div class="form-block">
            {!! Form::model($user, ['method' => 'POST', 'route' => ['user.edit.post', $user->id]]) !!}
            @include('user/partials/_form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection