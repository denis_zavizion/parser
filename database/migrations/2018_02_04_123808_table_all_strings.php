<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAllStrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_all_strings', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('active_second')->unsigned();
            $table->boolean('is_active_today')->nullable();
            $table->string('string');
            $table->string('string_table');
            $table->string('string_header');
            $table->json('parser_status');
            $table->integer('country_id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('links_per_page')->unsigned();
            $table->integer('pages_count')->unsigned();
            $table->json('language')->nullable();
            $table->string('interface_language')->nullable();
            $table->tinyInteger('current_status')->unsigned()->default(1);
            $table->tinyInteger('current_status_string')->unsigned()->default(1);
            $table->json('date_scheduled');
            $table->timestamp('last_parsing_date')->nullable();
            $table->timestamp('next_parsing_date')->nullable();
            $table->timestamps();

            $table->index('user_id');
            $table->index('country_id');
            $table->index('city_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('countries');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_all_strings', function (Blueprint $table) {
            $table->dropForeign('table_all_strings_user_id_foreign');
            $table->dropForeign('table_all_strings_country_id_foreign');;
        });
        Schema::dropIfExists('table_all_strings');
    }
}
