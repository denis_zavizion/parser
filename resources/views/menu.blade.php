<header id="header" class="clearfix">

    <nav id="navmobile-btn"><i class="fa fa-navicon"></i></nav>

    <ul class="main-menu">
        <li>
            <a href="{{ route('home') }}">« ГЛАВНАЯ СТРАНИЦА</a>
        </li>
        <li class=" @if ($page == 'monitoring.new') active @endif">
            <a href="{{ route('monitoring.new') }}">НОВЫЙ МОНИТОРИНГ</a>
        </li>
        <li class=" @if ($page == 'monitoring.index') active @endif">
            <a href="{{ route('monitoring.index') }}">МОНИТОРИНГ</a>
        </li>
        <li class=" @if ($page == 'user.settings') active @endif">
            <a href="{{ route('user.edit', auth()->user()->id) }}">НАСТРОЙКИ</a>
        </li>
        @if (auth()->user()->isAdmin())
            <li class=" @if ($page == 'country.index') active @endif">
                <a href="{{ route('country.index', auth()->user()->id) }}">СТРАНЫ</a>
            </li>
            <li class=" @if ($page == 'user.index') active @endif">
                <a href="{{ route('user.index') }}">ПОЛЬЗОВАТЕЛИ</a>
            </li>
        @endif
    </ul>

    <ul class="right-menu">
        <li class="user-link">
            <a href="{{ route('user.edit', auth()->user()->id) }}">{{ auth()->user()->login }}<i class="fa fa-user-circle-o"></i></a>
        </li>
        <li class="logout-link">
            <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i></a>
        </li>
    </ul>

</header>