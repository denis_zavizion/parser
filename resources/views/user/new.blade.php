@extends('app')

@section('content')
    @include('menu', ['page' => 'user.new'])
    <div id="container">
        <div class="form-block">
            {!! Form::model($user, ['method' => 'POST', 'route' => ['user.new.post']]) !!}
                @include('user/partials/_form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection