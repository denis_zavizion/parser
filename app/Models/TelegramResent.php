<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class TelegramResent extends Model {

	protected $table = 'telegram_resent';
	public $timestamps = false;

	protected $fillable = [
		'chat_id',
		'resent_second',
		'monitoring_id',
	];

	/*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
	public function telegramAccount()
	{
		return $this->belongsTo('App\TelegramAccount');
	}
	
}
