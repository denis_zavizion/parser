<?php
use App\Models\Monitoring;

/**
 * Created by PhpStorm.
 * User: vim
 * Date: 02.02.18
 * Time: 22:40
 */
/// date("Y-m-d H:i:s");
if (!function_exists('example')) {
    function example()
    {
        return 'example';
    }
}

if (!function_exists('dayNow')) {
    function dayNow()
    {
        return date("Y-m-d");
    }
}

if (!function_exists('yesterday')) {
    function yesterday()
    {
        return date("Y-m-d", strtotime("-1 day"));
    }
}

if (!function_exists('tomorrow')) {
    function tomorrow()
    {
        return date("Y-m-d", strtotime("+1 day"));
    }
}

if (!function_exists('timeNow')) {
    function timeNow()
    {
        return date("Y-m-d H:i:s");
    }
}

if (!function_exists('timeTomorrow')) {
    function timeTomorrow()
    {
        ///$d=strtotime("+1 day");
        ///echo date("Y-m-d",$d);
        /// ("d.m.Y", time() + 3600);
        return date("Y-m-d H:i:s", time() + 3600*24);
    }
}

if (!function_exists('domain')) {
    function domain($url)
    {
        return parse_url($url, PHP_URL_HOST);;
    }
}

/// сколько секунд от полуночи сегодня
if (!function_exists('todaySecondNow')) {
    function todaySecondNow()
    {
        return timeToSecond(date('H:i:s'));
    }
}

/// день недели "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
if (!function_exists('weekdayNow')) {
    function weekdayNow()
    {
        return date('l');
    }
}

if (!function_exists('roundH')) {
    function roundH($num)
    {
        return round($num);
    }
}

if (!function_exists('printEm')) {
    function printEm($str)
    {
        $str = explode('<br>', $str)[0];
        $str = strip_tags($str, '<em>');
        $str = explode('...', $str)[0];
        $str = explode('http', $str)[0];
        $str = str_limit($str, $limit = 333, $end = '');
        return $str;
    }
}

if (!function_exists('linkToPage')) {
    function linkToPage($link_count, $page_size = 100)
    {

        $additional_page = 0;

        if (($link_count % $page_size) > 0) {
            $additional_page = 1;
        }

        return round( $link_count/ $page_size ) +  $additional_page ;

    }
}

/// в секунды "14:03:07" or "14:17" to 4675
if (!function_exists('timeToSecond')) {
    function timeToSecond($str)
    {
        $arr = explode(":", $str);

        $sum = (int)$arr[0]*3600;

        $sum += (int)$arr[1]*60;

        if (isset($arr[2])) {
            $sum +=  (int)$arr[2];
        }

        return $sum;
    }
}

if (!function_exists('newMonitoringsCount')) {
    function newMonitoringsCount($monitoring, $type)
    {
        $count = $monitoring->getLnksForDay(dayNow(), $type)
            ->where([
                'mark' => Monitoring::MARK_NEW,
            ])
            ->count()
        ;
        return  $count;
    }
}

if (!function_exists('generateToken')) {
    function generateToken($id)
    {
        $hashids = new \Hashids\Hashids('fkjsdhfhwekjfhwefhvnnverherwqfghvfg');
        return str_random(1).$hashids->encode($id+50).str_random(1);
    }
}

if (!function_exists('secondToTime')) {
    function secondToTime($second)
    {
        return str_pad(round(($second-$second % 3600)/3600), 2, '0', STR_PAD_LEFT) 
		.':'
		.str_pad(round($second % 3600/60), 2, '0', STR_PAD_LEFT);
    }
}
