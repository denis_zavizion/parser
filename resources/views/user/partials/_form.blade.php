<div class="form-row">
    <label class="form-label"><span>Логин</span></label>
    <div class="inp-box">
        {!! Form::text('login', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('login')) <p class="error">{{ $errors->first('login') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Пароль</span></label>
    <div class="inp-box">
        {!! Form::password('new_password', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('new_password')) <p class="error">{{ $errors->first('new_password') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Е-Мейл</span></label>
    <div class="inp-box">
        {!! Form::text('email', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('email')) <p class="error">{{ $errors->first('email') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Имя</span></label>
    <div class="inp-box">
        {!! Form::text('name', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('name')) <p class="error">{{ $errors->first('name') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Фамилия</span></label>
    <div class="inp-box">
        {!! Form::text('surname', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('surname')) <p class="error">{{ $errors->first('surname') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Должность, компания</span></label>
    <div class="inp-box">
        {!! Form::text('cv', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('cv')) <p class="error">{{ $errors->first('cv') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Телефон</span></label>
    <div class="inp-box">
        {!! Form::text('tel', null, ['maxlength' => '255', 'class' => 'input-text']) !!}
        @if ($errors->has('tel')) <p class="error">{{ $errors->first('tel') }}</p> @endif
    </div>
</div>
<div class="form-row">
    <label class="form-label"><span>Телеграм бот</span></label>
    <div class="inp-box">{{ env('TELEGRAM_BOT_NAME') }}</div>
</div>
@if($user->created_at)
    <div class="form-row">
        <label class="form-label"><span>Токен владельца</span></label>
        <div class="inp-box token">
            {{ $user->telegram->token }} 
            <span>{{ $user->telegram->chat_name }}</span>
            <a onclick="return confirm('Вы уверены?')" 
            href="{{ route('user.token.clear', $user->telegram->id) }}" class="remove-link"></a>
        </div>
    </div>
    @foreach($user->guests as $guest)
        <div class="form-row">
            <label class="form-label"><span>Гостевой токен </span></label>
            <div class="inp-box token">
                {{  $guest->token }} <span>{{ $guest->chat_name }}</span>
                <a onclick="return confirm('Вы уверены?')" 
            href="{{ route('user.token.clear', $guest->id) }}" class="remove-link"></a>
        
                </div>
        </div>
    @endforeach
@endif
<div class="submit-row">
    <div class="inp-box">
        <input type="submit" class="btn" value="Сохранить">
    </div>
</div>
