<?php

namespace App\Http\Controllers;

use App\Models\Monitoring;

class HomeController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $monitorings = Monitoring::where([
            'user_id' => $user->id,
            'current_status_string' => Monitoring::STATUS_ACTIVE,
        ])
            ->limit(10)
            ->latest()
            ->get()
        ;

        if ($monitorings) {

            return view('monitoring.home_index', compact('monitorings'));
        }
        return view('home');
    }

}
