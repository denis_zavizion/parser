<div class="graph-column">
    <div id="graph-{{ $minitoring->id }}" class="graph-marks"></div>
    <script>
        var days = {{ $minitoring->graphCategories()}};

        chart = Highcharts.chart('graph-'+{{ $minitoring->id }}, {
            title: {
                text: false
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            chart : {
                backgroundColor: 'transparent',
                style: {"fontFamily":"\"Ubuntu\", sans-serif","fontSize":"14px","fontWeight":"300"}
            },
            plotOptions: {
                line: {
                    color: '#08a43b',
                    marker: {
                        fillColor: '#081724',
                        lineColor: '#08a43b',
                        lineWidth: 2,
                        radius: 5
                    }
                }
            },
            series: [{
                label: {
                    enabled: false
                },
                line: {
                    color: '#08a43b'
                },
                name: "{{ $minitoring->string_header }}",
                data: {{ $minitoring->graph($type) }}
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 600
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            },
            xAxis: {
                lineWidth: 0,
                labels: {
                    style: {
                        color: '#999999',
                        fontSize: '12px'
                    }
                },
                categories: days,
            },
            yAxis: {
                gridLineColor: '#1a2732',
                labels: {
                    style: {
                        color: '#999999',
                        fontSize: '12px'
                    }
                },
                min: 0,
                max: 100,
                title: {
                    enabled: false
                }
            }

        });
    </script>
</div>