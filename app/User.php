<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\Models\TelegramAccount;
use App\Models\Monitoring;

class User extends Authenticatable
{
    use Notifiable;

    const  ROLE_ADMIN = 1;
    const  ROLE_USER = 0;

    const  STATUS_ACTIVE = 1;
    const  STATUS_INACTIVE  = 0;

    public $new_password;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'password',
        'surname',
        'name',
        'cv',
        'tel',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        return $this->role == self::ROLE_ADMIN;
    }

    protected $attributes = [
        'role' => self::ROLE_USER,
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    public function telegrams()
    {
        return $this->hasMany(\App\Models\TelegramAccount::class, 'user_id', 'id');
    }
    public function telegram()
    {
        return $this->hasOne(\App\Models\TelegramAccount::class, 'user_id', 'id')
        ->where('is_guest', false);
    }

    public function monitorings()
    {
        return $this->hasMany(\App\Models\Monitoring::class, 'user_id', 'id');
    }
    public function guests()
    {
        return $this->hasMany(\App\Models\TelegramAccount::class, 'user_id', 'id')
        ->where('is_guest', true);;
    }
    public function activeMonitorings()
    {
        return $this->hasMany(\App\Models\Monitoring::class, 'user_id', 'id')
        ->where('current_status_string', Monitoring::STATUS_ACTIVE);
    }

    /*
    |--------------------------------------------------------------------------
    | Validation Rules
    |--------------------------------------------------------------------------
    */

    public static function createRules()
    {
        $rules = [
            'login' => 'required|unique:users|max:255',
            'email' => 'required|email',
            'new_password' => '|min:6|max:32',
        ];

        return $rules;
    }

    public static function editRules($user)
    {
        $rules = [
            'login' => 'required|unique:users,login,'.$user->id,
            'email' => 'required|email',
            'new_password' => 'sometimes|nullable|min:6|max:32',
        ];

        return $rules;
    }

   
    /*
    |--------------------------------------------------------------------------
    | Metods
    |--------------------------------------------------------------------------
    */



    public function delete()
    {
        foreach($this->telegrams as $telegram) {
            $telegram->delete();
        }
        foreach($this->monitorings as $monitoring) {
            $monitoring->delete();
        }
        return parent::delete();

    }


    public function  linkMark($link, $parsed_by, $day, $mark)
    {
        foreach ($this->monitorings as $monitoring) {
            DB::table('string_'.$monitoring->id)
            ->where('link', $link)
            ->where('parsed_by', $parsed_by)
            ->whereDate('parsing_date', '=', $day)
            ->update([
                'mark' => $mark,
            ])
            ;
        }

        $this->buildStatistics($day);

    }

    public function  buildStatistics($day)
    {
        foreach ($this->monitorings as $monitoring) {
            $monitoring->buildStatistics($day);
        }

    }

    public static function adminEmail()
    {
        $admin = User::where(['login' => 'admin'])->first();

        return $admin->email;
    }

    public function createTelegramAccount()
    {

        if (!$this->telegram) {

            $newTelegram = new TelegramAccount([
                'user_id' => $this->id,
                'token' => generateToken($this->id*100),
                'is_guest' => false,
                'is_auto' =>  false,
                'auto_second' => 0,
                'auto_period' => 0,
                'auto_trigger' => 0,
                'previous_command' =>  'none' ,
            ]);

            $newTelegram->save();

            return $newTelegram->token;

        }

        return $this->telegram->token;
    }

    public function createGuestAccount()
    {
        $count = $this->telegrams()->count();
        if ($count < 100) {
            $newTelegram = new TelegramAccount([
                'user_id' => $this->id,
                'token' => generateToken($this->id*100 + $count),
                'is_guest' => true,
                'is_auto' =>  false,
                'auto_second' => 0,
                'auto_period' => 0,
                'auto_trigger' => 0,
                'previous_command' =>  'none' ,
            ]);
            $newTelegram->save();
            return $newTelegram->token;
        }

        return 'Вы исчерпали лимит гостевых токенов = '.$count;

        
    }

}
